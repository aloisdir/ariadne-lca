from premise.utils import eidb_label
from lca2rmnd.utils import project_string
from lcatool import scenarios, years, mifs
from lcatool import get_storage_file_path

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import plotly.express as px
import pandas as pd
import numpy as np
import seaborn as sns
import brightway2 as bw
from cycler import cycler
import os
import bw2analyzer as bw2a

plot_folder = "plots/ilcd/"
region = "EUR"
# size = "Medium"
model = "remind"

selected_methods = [
    ('ILCD 2.0 2018 midpoint', 'human health', 'non-carcinogenic effects'),
    ('ILCD 2.0 2018 midpoint', 'human health', 'carcinogenic effects'),
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'freshwater ecotoxicity'),
    ('ILCD 2.0 2018 midpoint', 'resources', 'minerals and metals'),
    ('ILCD 2.0 2018 midpoint', 'resources', 'land use')
]

units = {
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'freshwater ecotoxicity'): "CTUe",
    ('ILCD 2.0 2018 midpoint', 'human health', 'non-carcinogenic effects'): "CTUh",
    ('ILCD 2.0 2018 midpoint', 'human health', 'carcinogenic effects'): "CTUh",
    ('ILCD 2.0 2018 midpoint', 'resources', 'minerals and metals'): "kg Sb eq",
    ('ILCD 2.0 2018 midpoint',
     'ecosystem quality',
     'freshwater and terrestrial acidification'): "mol H+ eq",
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'freshwater eutrophication'): "kg P eq",
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'marine eutrophication'): "kg N eq",
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'terrestrial eutrophication'): "mol N eq",
    ('ILCD 2.0 2018 midpoint', 'human health', 'ionising radiation'): "kBq U235",
    ('ILCD 2.0 2018 midpoint', 'human health', 'ozone layer depletion'): "kg CFC-11eq",
    ('ILCD 2.0 2018 midpoint', 'human health', 'photochemical ozone creation'): "kg NMVOC eq",
    ('ILCD 2.0 2018 midpoint', 'human health', 'respiratory effects, inorganics'): "Disease incidences",
    ('ILCD 2.0 2018 midpoint', 'resources', 'dissipated water'): "m3 water",
    ('ILCD 2.0 2018 midpoint', 'resources', 'fossils'): "MJ",
    ('ILCD 2.0 2018 midpoint', 'resources', 'land use'): "dim. less index",
    ('ILCD 2.0 2018 midpoint', 'climate change', 'climate change total'): "kg CO2 eq",
}

norm_fac = {
    ('ILCD 2.0 2018 midpoint', 'climate change', 'climate change total'): 5.2e2,
    ('ILCD 2.0 2018 midpoint', 'human health', 'photochemical ozone creation'): 3.8,
    ('ILCD 2.0 2018 midpoint', 'human health', 'ozone layer depletion'): 7.8e-2,
    ('ILCD 2.0 2018 midpoint',
     'ecosystem quality',
     'freshwater and terrestrial acidification'): 2.3e3,
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'terrestrial eutrophication'): 2.8e3,
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'freshwater eutrophication'): 8.4e-1,
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'marine eutrophication'): 2.9e1,
    ('ILCD 2.0 2018 midpoint', 'ecosystem quality', 'freshwater ecotoxicity'): 1.9e4,
    ('ILCD 2.0 2018 midpoint', 'resources', 'dissipated water'): 3e3, # m3 -> kg
}

methods = [str(a) for a in norm_fac]

ldv_str = "ILCD_LDV_LCA"

# population data
pop_data = pd.read_csv(mifs[0], sep=";").drop(columns=["Model", "Scenario"])
if(len(pop_data.columns == 20)):
    pop_data.drop(columns=pop_data.columns[-1], inplace=True)
pop_data = pop_data[
    (pop_data["Variable"] == "Population") &
    (pop_data["Region"] == "EUR")].drop(columns=["Unit", "Region", "Variable"])
pop_data = pop_data.melt(var_name="Year").astype({"Year": int})
pop_data = pop_data[pop_data["Year"].isin(years)]
pop_data.set_index("Year", inplace=True)

def plot_midpoint_scen_pkm():
    # midpoint, performance accross scenarios, per pkm
    with PdfPages(
            os.path.join(
                plot_folder,
                "scenario_performance_midpoint_LDV_fleet_pkm.pdf")) as pdf:

        data = pd.concat((
            pd.read_csv(
                get_storage_file_path(sc, ldv_str),
                index_col=["Region", "Method", "Year"])
            .loc[region]
            .assign(Scenario=sc)
            for sc in scenarios))

        # data = data[data.Variable.str.contains(size)]
        data.Variable = data.Variable.str.replace(
            "ES\|Transport\|VKM\|Pass\|Road\|LDV\|", "")

        for method in methods:
            fig, axs = plt.subplots(
                4, 2,
                sharex=True, sharey=True,
                figsize=[12, 16])
            fig.suptitle(
                "Impact per pkm, {}: LDV fleet in {}"
                .format(eval(method)[2], region))
            toplot = data.loc[method]
            toplot.reset_index(inplace=True)
            toplot.set_index(["Variable", "Year"], inplace=True)
            varis = toplot.index.levels[0]

            for idx, var in enumerate(varis):
                ax = axs[int(idx / 2), idx % 2]
                ax.set_ylabel(units[eval(method)])
                vardt = toplot.loc[var]
                vardt = vardt.pivot(columns="Scenario", values="score_pkm")
                vardt.plot(ax=ax, title=var, legend=False)
                handles, labels = ax.get_legend_handles_labels()

            fig.legend(handles, labels, loc="upper right")
            pdf.savefig()
            plt.close()


def plot_midpoint_scen_total():
    # midpoint, performance accross scenarios, fleet total
    with PdfPages(
            os.path.join(
                plot_folder,
                "scenario_performance_midpoint_LDV_fleet_total.pdf")) as pdf:

        data = pd.concat((
            pd.read_csv(
                get_storage_file_path(sc, ldv_str),
                index_col="Region")
            .loc[region]
            .assign(Scenario=sc)
            for sc in scenarios))

        data = data.groupby(["Method", "Year", "Scenario"]).sum()

        fig, axs = plt.subplots(4, 4, sharex=True, figsize=[14, 16])
        fig.suptitle("Full fleet, total impact in {}"
                     .format(region))
        for idx, method in enumerate(methods):
            toplot = data.loc[method]
            toplot.reset_index(inplace=True)
            toplot.set_index("Year", inplace=True)

            ax = axs[int(idx / 4), idx % 4]
            ax.set_ylabel(units[eval(method)])

            vardt = toplot.pivot(columns="Scenario", values="total_score")
            vardt.plot(ax=ax, title=eval(method)[2], legend=False)
            handles, labels = ax.get_legend_handles_labels()

        fig.legend(handles, labels, loc="upper right")
        fig.tight_layout()
        pdf.savefig()
        plt.close()


def plot_midpoint_tech_pkm():
    # relative midpoint performance of technolgies
    scens = [sc for sc in scenarios if "Budg1100" in sc
             and "_LowD" not in sc]
    with PdfPages(
            os.path.join(
                plot_folder,
                "relative_performance_LDVs.pdf")) as pdf:
        for scenario in scens:
            test = pd.read_csv(get_storage_file_path(scenario, ldv_str))
            test.set_index(["Region", "Method", "Year"], inplace=True)
            dt = test.loc[region]

            fig, axs = plt.subplots(4, 4, sharex=True, figsize=[14, 16])

            fig.suptitle("{}: Fleet in {}"
                         .format(scenario, region))
            for idx, method in enumerate(methods):
                ax = axs[int(idx / 4), idx % 4]
                ax.set_ylabel(units[eval(method)])

                toplot = dt.loc[method]
                # toplot = toplot[toplot.Variable.str.contains(size)]
                toplot = toplot.pivot(columns="Variable", values="score_pkm")
                toplot.plot(ax=ax, title=eval(method)[2], legend=False)
                handles, labels = ax.get_legend_handles_labels()

            fig.legend(handles, labels, loc="upper right")
            fig.tight_layout()
            pdf.savefig()
            plt.close()


def plot_midpoint_scen_total_norm_bar(year=2030):
    # Midpoint impacts normalized to Base

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, ldv_str),
            index_col=["Year", "Region"])
        .loc[(year, region)]
        .assign(Scenario=sc)
        for sc in scenarios))

    # fleet totals
    data = data.groupby(["Scenario", "Method"]).sum()

    # normalize
    data = (data / data.loc["Budg1100_Conv"] - 1)
    data.reset_index(inplace=True)

    data["Method"] = data["Method"].apply(lambda x: eval(x)[2])
    data = data.where(data["Scenario"] != "Budg1100_Conv").dropna()
    data.set_index("Method", inplace=True)
    vardt = data.pivot(columns="Scenario", values="total_score")

    vardt.plot.barh(
        title=("Midpoint Impacts relative to the Conv scenario, {}"
               .format(year)),
        figsize=(12, 12))
    plt.xscale("symlog")
    plt.axvline(0)
    plt.xticks([-0.5, -0.25, 0, 0.25, 0.5, 1, 2],
               [50, 75, 100, 125, 150, 200, 300])
    plt.xlabel("%")
    plt.tight_layout()
    fname = ("scenario_performance_midpoint_fullfleet_{}_bar.pdf"
             .format(year))
    plt.savefig(
        os.path.join(
            plot_folder, fname))


def plot_selected_midpoint_total_bar(year=2030):
    # Midpoint impacts normalized to Base

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, ldv_str),
            index_col=["Year", "Region"])
        .loc[(year, region)]
        .assign(Scenario=sc)
        for sc in scenarios))


    # fleet totals
    data = data.groupby(["Scenario", "Method"]).sum()

    data.reset_index(inplace=True)
    data["Method"] = data["Method"].apply(eval)

    data.set_index("Method", inplace=True)
    data = data.loc[selected_methods]

    data = data.reset_index().set_index(
        ["Scenario", "Method"])

    # data = data.query("Scenario != 'NPi_Conv'")
    # normalize
    data = (data / data.loc["Budg1100_Conv"] - 1)
    data = data.query("Scenario != 'Budg1100_Conv'")
    data.reset_index(inplace=True)

    data["Method"] = data["Method"].apply(lambda x: "{}\n{}".format(
        x[1].capitalize(), x[2].capitalize()))

    data.set_index("Method", inplace=True)
    vardt = data.pivot(columns="Scenario", values="total_score")

    methods = ["{}\n{}".format(m[1].capitalize(), m[2].capitalize()) for m in selected_methods]
    method_mapping = {method: i for i, method in enumerate(methods)}
    key = vardt.index.map(method_mapping)
    vardt = vardt.iloc[key.argsort()]
    vardt.plot.barh(
        title=("Midpoint Impacts relative to the Conv scenario, {}"
               .format(year)),
        figsize=(9, 5))
    plt.xscale("symlog")
    plt.axvline(0)
    plt.xticks([-0.5, -0.25, 0, 0.25, 0.5, 1],
               [50, 75, 100, 125, 150, 200])
    plt.xlabel("%")
    plt.tight_layout()
    fname = ("scenario_performance_selected_midpoint_fullfleet_{}_bar.png"
             .format(year))
    plt.savefig(
        os.path.join(
            plot_folder, fname))


def plot_contribution_analysis_treemap():

    year = 2050
    scencars = {
        "Budg1100_FC": "Passenger car, FCEV, Medium",
        "Budg1100_IC": "Passenger car, ICEV-d, Medium",
        "Budg1100_BE": "Passenger car, BEV, Medium",
        "Budg1100_ICsyn": "Passenger car, ICEV-d, Medium",
    }

    methods = [
        ('ReCiPe Endpoint (H,A) (obsolete)', 'resources', 'total'),
        ('ReCiPe Endpoint (I,A) (obsolete)', 'ecosystem quality', 'total'),
        ('ReCiPe Endpoint (I,A) (obsolete)', 'human health', 'total')
    ]

    for scenario, acstr in scencars.items():
        for method in methods:
            bw.projects.set_current(project_string(scenario))
            db = bw.Database(eidb_label(model, scenario, year))

            # find activity
            act = [a for a in db if
                   a["name"].startswith(acstr)
                   and a["location"] == region][0]

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            tot_score = lca.score

            results = []

            for exc in act.technosphere():
                if exc.input["name"] == "polyethylene production, high density, granulate":
                    exc["tag"] = "energy storage"
                lca.redo_lcia({exc.input: exc['amount']})
                results.append((exc["tag"], exc.input["name"], lca.score))

            results.append(
                ("direct", "driving emissions",
                 tot_score - sum(p[2] for p in results)))

            df = pd.DataFrame.from_records(
                results,
                columns=["parents", "children", "values"])

            df["all"] = act["name"]  # in order to have a single root node
            # remove negative values
            df = df[df["values"] > 0]

            # df.set_index(["all", "parent", "children"], inplace=True)
            fig = px.treemap(
                df, path=['all', 'parents', 'children'], values='values')
            # fig.show()
            fig.write_image(
                os.path.join(
                    plot_folder,
                    "treemap_{}_{}.png".format(method[1], scenario)),
                width=1024, height=768, scale=4)


def plot_contribution_analysis_stackedbars():

    years = [2030, 2050]
    scencars = {
        "Budg1100_H2Push": "transport, passenger car, fleet average, FCEV",
        "Budg1100_Conv": "transport, passenger car, fleet average, ICEV-p",
        "Budg1100_ElecPush": "transport, passenger car, fleet average, BEV",
        "Budg1100_ConvSyn": "transport, passenger car, fleet average, ICEV-p",
    }

    scen_techs = {
        "Budg1100_H2Push": "H2Push/FCEV",
        "Budg1100_Conv": "Conv/ICEV-p",
        "Budg1100_ElecPush": "ElecPush/BEV",
        "Budg1100_ConvSyn": "ConvSyn/ICEV-p",
    }

    with PdfPages(
            os.path.join(
                plot_folder,
                "selected_midpoint_contributions_stacked_bars.pdf")) as pdf:

        for method in selected_methods:

            fig, axs = plt.subplots(
                1, len(years), sharey=True, sharex=True, figsize=[12, 5])
            for idx, year in enumerate(years):
                results = []

                for scenario, acstr in scencars.items():
                    bw.projects.set_current(project_string(scenario))
                    db = bw.Database(eidb_label(model, scenario, year))

                    # find activity
                    act = [a for a in db if
                           a["name"].startswith(acstr)
                           and a["location"] == region][0]

                    lca = bw.LCA({act: 1}, method)
                    lca.lci()
                    lca.lcia()

                    tot_score = lca.score
                    proc_score = []

                    for exc in act.technosphere():
                        if exc.input["name"] == "polyethylene production, high density, granulate":
                            exc["tag"] = "energy storage"
                        lca.redo_lcia({exc.input: exc['amount']})
                        results.append(
                            (scen_techs[scenario], exc["tag"],
                             exc.input["name"], lca.score))
                        proc_score.append(lca.score)

                    results.append(
                        (scen_techs[scenario], "direct", "driving emissions",
                         tot_score - sum(proc_score)))

                df = pd.DataFrame.from_records(
                    results,
                    columns=["scenario/drivetrain", "tag", "activity", "values"])

                ## stacked bars
                res = df.groupby(["scenario/drivetrain", "tag"]).sum().unstack()
                res.columns = res.columns.get_level_values(1)

                ax = axs[idx]

                ax.set_ylabel(units[method])

                res.plot(
                    ax=ax, title="{}, {}".format(method[2], year),
                    kind="bar", stacked=True, legend=False)
                handles, labels = ax.get_legend_handles_labels()

            lgd = fig.legend(handles, labels, loc="center",
                             bbox_to_anchor=(1, 0.5))
            # plt.tight_layout()
            pdf.savefig(bbox_inches="tight")
            plt.close()


def plot_contribution_analysis_stackedbars_relative(year = 2050):

    scencars = {
        "Budg1100_H2Push": "transport, passenger car, fleet average, FCEV",
        "Budg1100_Conv": "transport, passenger car, fleet average, ICEV-p",
        "Budg1100_ElecPush": "transport, passenger car, fleet average, BEV",
        "Budg1100_ConvSyn": "transport, passenger car, fleet average, ICEV-p",
    }

    scen_techs = {
        "Budg1100_H2Push": "FCEV",
        "Budg1100_Conv": "ICEV(petrol)",
        "Budg1100_ElecPush": "BEV",
        "Budg1100_ConvSyn": "ICEV(synth. petrol)",
    }

    results = []
    # reference activity
    ref_scenario = "Budg1100_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))
    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"].startswith(scencars[ref_scenario])
        and a["location"] == region][0]

    for method in selected_methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        tot_score = lca.score
        proc_score = []

        for exc in act.technosphere():
            if exc.input["name"] == "polyethylene production, high density, granulate":
                exc["tag"] = "energy storage"
            lca.redo_lcia({exc.input: exc['amount']})
            results.append(
                (ref_string, method, exc["tag"],
                 exc.input["name"], lca.score))
            proc_score.append(lca.score)

        results.append(
            (ref_string, method, "direct", "driving emissions",
             tot_score - sum(proc_score)))


    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"].startswith(acstr)
               and a["location"] == region][0]
        for method in selected_methods:

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            tot_score = lca.score
            proc_score = []

            for exc in act.technosphere():
                if exc.input["name"] == "polyethylene production, high density, granulate":
                    exc["tag"] = "energy storage"
                lca.redo_lcia({exc.input: exc['amount']})
                results.append(
                    (scen_techs[scenario], method, exc["tag"],
                     exc.input["name"], lca.score))
                proc_score.append(lca.score)

            results.append(
                (scen_techs[scenario], method, "direct", "driving emissions",
                 tot_score - sum(proc_score)))

    df = pd.DataFrame.from_records(
        results,
        columns=["scenario", "method", "tag", "activity", "values"])
    df.reset_index(inplace=True)
    df["method"] = df["method"].apply(lambda x: "{}, {}".format(
        x[1].capitalize(), x[2]))

    df.to_csv("output/scenario_comparison_tagged.csv")
    ## plot in R


def plot_tech_heatmap_relative(year = 2050):

    scencars = {
        "Budg1100_H2Push": "transport, passenger car, fleet average, FCEV",
        "Budg1100_Conv": "transport, passenger car, fleet average, ICEV-p",
        "Budg1100_ElecPush": "transport, passenger car, fleet average, BEV",
        "Budg1100_ConvSyn": "transport, passenger car, fleet average, ICEV-p",
    }

    scen_techs = {
        "Budg1100_H2Push": "FCEV",
        "Budg1100_Conv": "ICEV(petrol)",
        "Budg1100_ElecPush": "BEV",
        "Budg1100_ConvSyn": "ICEV(synth. petrol)",
    }


    results = []
    # reference activity
    ref_scenario = "Budg1100_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))

    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"].startswith(scencars[ref_scenario])
        and a["location"] == region][0]

    for method in methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        results.append(
            (ref_string, method, lca.score))


    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"].startswith(acstr)
               and a["location"] == region][0]
        for method in methods:

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            results.append(
                (scen_techs[scenario], method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["technology", "method", "values"])
    df.reset_index(inplace=True)
    df["method"] = df["method"].apply(lambda x: "{}, {}".format(
        x[1].capitalize(), x[2]))
    df = df.set_index("method").drop(columns="index")

    scen_ord = ['BEV', 'FCEV', 'ICEV(synth. petrol)', 'ICEV(petrol)', ref_string]

    dfwide = df.pivot(columns="technology", values="values")[scen_ord]
    dfnorm = dfwide.div(dfwide[ref_string], axis="index").drop(columns=ref_string)
    fig, ax = plt.subplots(figsize=(8, 5))

    sns.heatmap(dfnorm, ax=ax, vmax=2, cmap='RdYlGn_r',
                annot=True, cbar=False, xticklabels=True, yticklabels=True)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=60, fontsize = 8)
    # ax.figure.subplots_adjust(bottom=0.2, left=0.2)
    fig.suptitle("Impacts in 2050 compared to an average car in 2020")
    fig.tight_layout()
    fname = ("midpoint_tech_heatmap_{}.png"
             .format(year))
    ax.figure.savefig(
        os.path.join(
            plot_folder, fname))



materials = ["Iron", "Aluminium", "Copper", "Gold", "Platinum"]

def plot_fleet_materials():
    from lcatool import years, mifs

    # per-capita consumption data from Kaplinsky, R. et al (2010).
    cons = {"Iron": 400, "Copper": 12, "Aluminium": 17}

    with PdfPages(
            os.path.join(
                plot_folder,
                "fleet_materials_line.pdf")) as pdf:
        for material in materials:
            fig, axs = plt.subplots(1, 2, figsize=[12, 5])

            data = pd.concat((
                pd.read_csv(
                    get_storage_file_path(sc, "materials"),
                    header=0,
                    names=["Year", "Region", "Material", "value"],
                    index_col=["Region", "Material", "Year"])
                .loc[(region, material)]
                .assign(Scenario=sc)
                for sc in scenarios))

            ax = axs[0]
            ax.set_ylabel("{} (kg)".format(material))
            # fleet totals
            data = data.pivot(columns="Scenario", values="value") * 1e-6 # population is in million
            # per capita
            data = data.div(pop_data["value"], axis="index")
            data.plot(ax=ax, title="{} demand per capita".format(material),
                      legend=False)
            if material in cons:
                ax.axhline(cons[material], color='r')

            ax = axs[1]
            ax.set_ylabel("{} (kg)".format(material))
            cum_data = pd.merge(
                pd.DataFrame({"Year": range(2020, 2050)}),
                data.reset_index(), how="outer").interpolate()
            cum_data = cum_data.set_index("Year").cumsum()

            cum_data.plot(
                ax=ax, title="{} demand, cumulative".format(material))
            plt.tight_layout()
            pdf.savefig()
            plt.close()


def plot_fleet_midpoint_per_capita():
    with PdfPages(
            os.path.join(
                plot_folder,
                "midpoint_perCapita_line.pdf")) as pdf:
        data = pd.concat((
            pd.read_csv(
                get_storage_file_path(sc, ldv_str),
                index_col="Region")
            .loc[region]
            .assign(Scenario=sc)
            for sc in scenarios))

        data = data.groupby(["Method", "Year", "Scenario"]).sum()

        fig, axs = plt.subplots(4, 4, sharex=True, figsize=[14, 16])
        fig.suptitle("Full fleet, per-capita impact in {}"
                     .format(region))
        for idx, method in enumerate(methods):
            toplot = data.loc[method]
            toplot.reset_index(inplace=True)
            toplot.set_index("Year", inplace=True)

            # fleet totals
            toplot = toplot.pivot(columns="Scenario", values="total_score") * 1e-6 # population is in million
            toplot = toplot.div(pop_data["value"], axis="index")

            ax = axs[int(idx / 4), idx % 4]
            ax.set_ylabel(units[eval(method)])

            toplot.plot(ax=ax, title=eval(method)[2], legend=False)
            handles, labels = ax.get_legend_handles_labels()
            # reference per-capita impact
            ax.axhline(norm_fac[eval(method)], color='r')

        fig.legend(handles, labels, loc="upper right")
        fig.tight_layout()
        pdf.savefig()
        plt.close()


def plot_fleet_midpoint_per_capita_normalized():
    with PdfPages(
            os.path.join(
                plot_folder,
                "midpoint_perCapita_normalized_line.pdf")) as pdf:
        data = pd.concat((
            pd.read_csv(
                get_storage_file_path(sc, ldv_str),
                index_col="Region")
            .loc[region]
            .assign(Scenario=sc)
            for sc in scenarios))

        data = data.groupby(["Method", "Year", "Scenario"]).sum()

        fig, axs = plt.subplots(4, 4, sharex=True, figsize=[14, 16])
        fig.suptitle("Full fleet, per-capita impact in {}"
                     .format(region))
        for idx, method in enumerate(methods):
            toplot = data.loc[method]
            toplot.reset_index(inplace=True)
            toplot.set_index("Year", inplace=True)

            # fleet totals
            toplot = toplot.pivot(columns="Scenario", values="total_score") * 1e-6 # population is in million
            toplot = toplot.div(pop_data["value"]*norm_fac[eval(method)], axis="index")

            ax = axs[int(idx / 4), idx % 4]
            ax.set_ylabel("impact rel. to avg. impact per capita")

            toplot.plot(ax=ax, title=eval(method)[2], legend=False)
            handles, labels = ax.get_legend_handles_labels()

        fig.legend(handles, labels, loc="upper right")
        fig.tight_layout()
        pdf.savefig()
        plt.close()


def plot_fleet_midpoint_per_capita_bar_normalized(year=2050):
    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, ldv_str),
            index_col="Region")
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns="score_pkm")

    data = data.groupby(["Year", "Method", "Scenario"]).sum("total_score")
    data = data.merge(pop_data, left_index=True, right_index=True)
    data = data.loc[year]

    data.reset_index(inplace=True)
    data["Method"] = data["Method"].apply(eval)
    data["Scenario"] = data["Scenario"].str.replace("Budg1100_", "")

    data.set_index("Method", inplace=True)
    # data = data.loc[selected_methods]

    norms = pd.DataFrame.from_dict(norm_fac, orient="index",
                                   columns=["normfac"])

    data = data.merge(norms, left_index=True, right_index=True)
    data["percap_norm"] = data["total_score"] / (data["value"] * 1e6 * data["normfac"])
    data.index.name = "Method"
    data.drop(columns=["total_score", "value", "normfac"], inplace=True)

    data.reset_index(inplace=True)

    data["Category"] = data["Method"].apply(lambda x: x[1].capitalize())
    data["Method"] = data["Method"].apply(lambda x: x[2].capitalize())

    data = data[~data.Scenario.str.contains("_LowD")]
    scen_ord = ['ElecPush', 'H2Push', 'Conv', 'ConvSyn']

    vardt = data.pivot(index=["Category", "Method"], columns="Scenario", values="percap_norm")[scen_ord]

    fig, ax = plt.subplots(
        1, 4, sharey=True,
        figsize=(8, 5.5), gridspec_kw={'width_ratios': [0.1, 0.6, 0.5, 0.4]})
    # fig.suptitle("Impacts compared to an average car in 2020")

    for no, cat in enumerate(["Climate change", "Human health", "Ecosystem quality", "Resources"]):
        dfwide = vardt.loc[cat] * 100

        dfwide.plot.bar(ax=ax[no], width=0.8, legend=False)
        ax[no].axhline(100.)
        ax[no].set_xlabel("")
        ax[no].set_ylabel("rel. impact (%)")
        ax[no].yaxis.set_tick_params(length=0)
        ax[no].title.set_text(cat)
        # ax[no].set_xticklabels(
        #     ax[no].get_xticklabels(),
        #     rotation=45, ha="right")
        

    ax[0].yaxis.set_tick_params(length=2)
    handles, labels = ax[-1].get_legend_handles_labels()

    fig.legend(handles[:4], labels[:4], loc='lower left', bbox_to_anchor= (0.5, 0.75))
    fig.tight_layout()
    fname = ("ilcd_midpoint_tech_relative.png")
    fig.savefig(
        os.path.join(
            plot_folder, fname))

def plot_fleet_midpoint_heatmap_normalized(year=2050):
    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, ldv_str),
            index_col="Region")
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns="score_pkm")

    data = data.groupby(["Year", "Method", "Scenario"]).sum("total_score")
    data = data.merge(pop_data, left_index=True, right_index=True)
    data = data.loc[year]

    data.reset_index(inplace=True)
    data["Method"] = data["Method"].apply(eval)
    data["Scenario"] = data["Scenario"].str.replace("Budg1100_", "")

    data.set_index("Method", inplace=True)
    # data = data.loc[selected_methods]

    norms = pd.DataFrame.from_dict(norm_fac, orient="index",
                                   columns=["normfac"])

    data = data.merge(norms, left_index=True, right_index=True)
    data["percap_norm"] = data["total_score"] / (data["value"] * 1e6 * data["normfac"])
    data.index.name = "Method"
    data.drop(columns=["total_score", "value", "normfac"], inplace=True)

    data.reset_index(inplace=True)

    data["Method"] = data["Method"].apply(lambda x: "{}, {}".format(
        x[1].capitalize(), x[2]))

    data = data[~data.Scenario.str.contains("_LowD")]
    scen_ord = ['ElecPush', 'H2Push', 'ConvSyn', 'Conv']

    vardt = data.pivot(index="Method", columns="Scenario", values="percap_norm")[scen_ord]

    fig, ax = plt.subplots(figsize=(8, 5))

    sns.heatmap(vardt, vmax=2, ax=ax, cmap='RdYlGn_r',
                annot=True, cbar=False, xticklabels=True, yticklabels=True)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=60, fontsize = 8)
    # ax.figure.subplots_adjust(bottom=0.2, left=0.2)
    fig.suptitle("Fleet impacts in 2050 compared to avg. impacts per capita 2010 ")
    fig.tight_layout()
    fname = ("midpoint_fleet_heatmap_{}.png"
             .format(year))
    ax.figure.savefig(
        os.path.join(
            plot_folder, fname))


def plot_fleet_materials_normalized():
    materials = ["Iron", "Aluminium", "Gold", "Copper", "Platinum"]
    years = [2030, 2050]
    with PdfPages(
            os.path.join(
                plot_folder,
                "fleet_materials_normalized_bars.pdf")) as pdf:
        for material in materials:
            data = pd.concat((
                pd.read_csv(
                    get_storage_file_path(sc, "materials"),
                    header=0,
                    names=["Year", "Region", "Material", "value"],
                    index_col=["Region", "Material", "Year"])
                .loc[(region, material)]
                .assign(Scenario=sc)
                for sc in scenarios))

            # fleet totals
            data = data.pivot(columns="Scenario", values="value")
            data /= data.loc[2020]

            data.loc[years].plot(
                title="{} demand compared to 2020".format(material),
                kind="bar")
            pdf.savefig()
            plt.close()


if __name__ == '__main__':
    print("plot_fleet_midpoint_per_capita()")
    plot_fleet_midpoint_per_capita()
    # print("plot_fleet_midpoint_per_capita_normalized()")
    # plot_fleet_midpoint_per_capita_normalized()
    print("plot_selected_midpoint_total_bar(2050)")
    plot_selected_midpoint_total_bar(2050)
    # print("plot_midpoint_scen_total_norm_bar(2030)")
    # plot_midpoint_scen_total_norm_bar(2030)
    # print("plot_midpoint_scen_total_norm_bar(2050)")
    # plot_midpoint_scen_total_norm_bar(2050)
    # print("plot_midpoint_scen_pkm()")
    # plot_midpoint_scen_pkm()
    # print("plot_midpoint_scen_total()")
    # plot_midpoint_scen_total()
    # print("plot_midpoint_tech_pkm()")
    # plot_midpoint_tech_pkm()
    # print("plot_contribution_analysis_stackedbars()")
    # plot_contribution_analysis_stackedbars()
    # print("plot_fleet_materials()")
    # plot_fleet_materials()
    # print("plot_fleet_materials_normalized()")
    # plot_fleet_materials_normalized()
    print("plot_fleet_midpoint_per_capita_bar_normalized(2050)")
    plot_fleet_midpoint_per_capita_bar_normalized(2050)
    # print("plot_contribution_analysis_stackedbars_relative(year = 2050)")
    # plot_contribution_analysis_stackedbars_relative(year = 2050)
    # print("plot_tech_heatmap_relative(year = 2050)")
    # plot_tech_heatmap_relative(year = 2050)
    # print("plot_fleet_midpoint_heatmap_normalized(year=2050)")
    # plot_fleet_midpoint_heatmap_normalized(year=2050)
