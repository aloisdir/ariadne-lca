from premise.utils import eidb_label

from lcatool import region, scenarios, years, mifs, project_string
from lcatool import get_storage_file_path, mp2eps, ldv_fleet_files, remind_output_folder

import matplotlib.pyplot as plt

import pandas as pd
import numpy as np

import altair as alt
import brightway2 as bw
from cycler import cycler
import os
import bw2analyzer as bw2a

alt.renderers.enable('altair_viewer')

plt.style.use("ggplot")
plt.rcParams.update({'font.size': 10})
SMALL_SIZE = MEDIUM_SIZE = BIGGER_SIZE = 10
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title    fig.tight_layout()

plot_folder = "plots/"

# size = "Medium"
model = "remind"
budget = 900

ep_categories = ["human health", "ecosystem quality", "resources"]

units = {
    "human health": "DALYs",
    "ecosystem quality": "Species Loss",
    "resources": "US$2000"
}

climate_method = ("IPCC 2013", "climate change", "GWP 100a")

# population data
def get_pop_data():
    pop_data = pd.read_csv(mifs[0], sep=";").drop(columns=["Model", "Scenario"])
    if(len(pop_data.columns == 20)):
        pop_data.drop(columns=pop_data.columns[-1], inplace=True)
    pop_data = pop_data[
        (pop_data["Variable"] == "Population") &
        (pop_data["Region"] == "EUR")].drop(columns=["Unit", "Region", "Variable"])
    pop_data = pop_data.melt(var_name="Year").astype({"Year": int})
    pop_data = pop_data[pop_data["Year"].isin(years)]
    pop_data.set_index("Year", inplace=True)
    return pop_data


scencars = {
    "Synf_imp": "transport, passenger car, fleet average, gasoline",
    "Trend": "transport, passenger car, fleet average, gasoline",
    "Elec_dom": "transport, passenger car, fleet average, battery electric",
}

scen_techs = {
    "Synf_imp": "Otto (Synf_imp)",
    "Trend": "Otto (Trend)",
    "Elec_dom": "Elektro (Elec_dom)"
}

selected_methods = [
    ('ReCiPe Endpoint (H) V1.13', 'human health', 'human toxicity'),
    ('ReCiPe Endpoint (H) V1.13', 'human health', 'particulate matter formation'),
    ('ReCiPe Endpoint (H) V1.13', 'human health', 'photochemical oxidant formation'),
    ('ReCiPe Endpoint (H) V1.13', 'ecosystem quality', 'agricultural land occupation'),
    ('ReCiPe Endpoint (H) V1.13', 'ecosystem quality', 'urban land occupation'),
    ('ReCiPe Endpoint (H) V1.13', 'resources', 'metal depletion'),
]

methodmap = {
        "Freshwater ecotoxicity": "FW ecotox.",
        "Human toxicity": "Gifte",
        "Marine ecotoxicity": "Mar. ecotox.",
        "Terrestrial ecotoxicity": "Terr. ecotox.",
        "Metal depletion": "Metal depl.",
        "Fossil depletion": "Fossil depl.",
        "Water depletion": "Water depl.",
        "Freshwater eutrophication": "FW eutroph.",
        "Marine eutrophication": "Mar. eutroph.",
        "Ionising radiation": "Strahlung",
        "Ozone depletion": "Ozonschicht",
        "Terrestrial acidification": "Terr. acid.",
        "Photochemical oxidant formation": "Sommersmog",
        "Particulate matter formation": "Feinstaub",
        'Urban land occupation': "Urban land",
        'Agricultural land occupation': "Agric. land",
}

trans = {
    "Human health": "Gesundheitseffekte",
    "Ecosystem quality": "Umweltwirkungen",
    "Resources": "Resourcenverbräuche",
    "direct": "direkt",
    "energy chain": "Energieversorgung",
    "energy storage": "Batterie",
    "road": "Straße",
    "vehicle": "Fahrzeug"
}

tag_groups = {
    "direct": ["direct", "direct - non-exhaust"],
    "vehicle": ["EoL", "glider", "maintenance", "other", "powertrain"],
}
rev_tags = {
    lv: k for k,v in tag_groups.items() for lv in v
}


def plot_original_fleet_composition():
    fleet_folder = "~/remind/testruns/lca_paper/v5/"
    # load fleet files
    data = pd.concat((
        pd.read_csv(
            os.path.join(fleet_folder, "remind_{}_vintcomp.csv".format(sc)),
            names=["year", "region", "veh_size", "drivetrain", "vintage_year",
                     "total_demand", "vintage_demand"], index_col=["region"],
            skiprows=4)
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios))

    vintages = data.drop(columns=["vintage_year"]).groupby(["Scenario", "year", "veh_size", "drivetrain"])["vintage_demand"].sum("vintage_demand_vkm").to_frame("vintages")
    news = data.drop(columns=["vintage_year", "vintage_demand"]).drop_duplicates().set_index(["Scenario", "year", "veh_size", "drivetrain"])
    df = pd.merge(vintages, news, left_index=True, right_index=True)
    df["additions"] = df["total_demand"] - df["vintages"]
    df = df.reset_index().groupby(["Scenario", "year", "veh_size"]).sum().drop(columns=["total_demand"])
    df = pd.melt(df.reset_index(), id_vars=["Scenario", "year", "veh_size"], value_vars=["additions", "vintages"])
    df["value"] *= 1e-3
    df = df.query("year < 2060")

    df["Scenario"] = df["Scenario"].str.replace("Budg900_", "")
    ord_scens = ["ElecPush", "H2Push", "ConvSyn", "Conv",
                 "ElecPush_LowD", "H2Push_LowD", "ConvSyn_LowD", "Conv_LowD"]

    alt.Chart(df).mark_area().encode(
        x="year:O",
        y=alt.Y("value:Q", title="LDV Demand [billion vkm]"),
        color=alt.Color("veh_size", title="size"),
        opacity=alt.Opacity("variable")
    ).facet(
        alt.Facet("Scenario", sort=ord_scens),
        columns=4
    ).save(
        os.path.join(
            plot_folder, "demand_vintages_size_orignal.png"))


def plot_fleet_composition():
    # load fleet files
    data = pd.concat((
        pd.read_csv(
            os.path.join(ldv_fleet_files, "{}_vintcomp.csv".format(sc)),
            index_col=["IAM_region"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns=["Unnamed: 0"])

    data["fleet"] = "old"
    data.loc[data.eval("year == vintage_year"), "fleet"] = "new"
    df = data.drop(columns=["vintage_year"]).groupby(["Scenario", "year", "size", "fleet"]).sum("vintage_demand_vkm").reset_index()

    df["vintage_demand_vkm"] *= 1e-3 # billion VKM
    df["Scenario"] = df["Scenario"].str.replace("Budg900_", "")
    ord_scens = ["ElecPush", "H2Push", "ConvSyn", "Conv",
                 "ElecPush_LowD", "H2Push_LowD", "ConvSyn_LowD", "Conv_LowD"]

    df = df[df.year <= 2050]
    alt.Chart(df).mark_area().encode(
        x="year:O",
        y=alt.Y("vintage_demand_vkm:Q", title="LDV Demand [billion vkm]"),
        color=alt.Color("size"),
        opacity=alt.Opacity("fleet")
    ).properties(width=150, height=150).facet(
        alt.Facet("Scenario", sort=ord_scens),
        columns=4
    ).save(
        os.path.join(
            plot_folder, "demand_vintages_size.png"),
        scale_factor=2)

def plot_energy_investments():
    years = [2030, 2050]
    # load fleet files
    data = pd.concat((
        pd.read_csv(
            os.path.join(remind_output_folder, "remind_{}.mif".format(sc)),
            sep=";", index_col=["Region", "Variable"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns=["Model", "Unit", "Unnamed: 24"])

    variables = [
        "Energy Investments|Elec|Coal|w/ CCS",
        "Energy Investments|Elec|Coal|w/o CCS",
        "Energy Investments|Elec|Gas|w/ CCS",
        "Energy Investments|Elec|Gas|w/o CCS",
        "Energy Investments|Elec|Hydrogen",
        "Energy Investments|Elec|Oil",
        "Energy Investments|Elec|Biomass|w/ CCS",
        "Energy Investments|Elec|Biomass|w/o CCS",
        "Energy Investments|Elec|Nuclear",
        "Energy Investments|Elec|Solar",
        "Energy Investments|Elec|Wind",
        "Energy Investments|Elec|Hydro",
        "Energy Investments|Elec|Geothermal",
        "Energy Investments|Elec|Storage",
        "Energy Investments|Elec|Grid",
    ]

    data = data.loc[variables].reset_index()
    data = data.melt(id_vars=["Variable", "Scenario"], var_name="Year")

    data["Scenario"] = data["Scenario"].str.replace("Budg900_", "")
    data["Variable"] = data["Variable"].str.replace("Energy Investments\|Elec\|", "")

    ord_scens = ["Conv", "Conv_LowD", "ElecPush", "ElecPush_LowD", "H2Push", "H2Push_LowD",
                 "ConvSyn", "ConvSyn_LowD"]

    data.Year = data.Year.astype(int)
    data = data[data.Year.isin(years)]

    colors = {
        "Coal|w/ CCS": "#b2b2b2",
        "Coal|w/o CCS": "#0c0c0c",
        "Gas|w/ CCS": "#e5e5b2",
        "Gas|w/o CCS": "#999959",
        "Oil": "#cc7500",
        "Biomass|w/ CCS": "#33ff00",
        "Biomass|w/o CCS": "#005900",
        "Nuclear": "#ff33ff",
        "Solar": "#ffcc00",
        "Wind": "#337fff",
        "Hydro": "#191999",
        "Geothermal": "#e51900",
        "Hydrogen": "#66cccc",
        "Grid": "#7f7f7f",
        "Storage": "#9500da",
    }

    toplot = data[data.value > 0]

    alt.Chart(toplot).mark_bar().encode(
        x=alt.X("Scenario:O", sort=ord_scens),
        y=alt.Y("value:Q", title="Energy Investments [billion US$2005]"),
        color=alt.Color(
            "Variable:N", scale=alt.Scale(
                domain=list(colors.keys()), range=list(colors.values()))),
        column=alt.Column('Year:N', title="")
    ).properties(width=200, height=150).save(
        os.path.join(
            plot_folder, "energy_investments.png"),
        scale_factor=2)

def plot_fleet_composition_by_drivetrain():
    # load fleet files
    data = pd.concat((
        pd.read_csv(
            os.path.join(remind_output_folder, "remind_{}.mif".format(sc)),
            sep=";", index_col=["Region", "Variable"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns=["Model", "Unit", "Unnamed: 24"])

    variables = [
        "ES|Transport|VKM|Pass|Road|LDV|BEV",
        "ES|Transport|VKM|Pass|Road|LDV|FCEV",
        "ES|Transport|VKM|Pass|Road|LDV|Gases",
        "ES|Transport|VKM|Pass|Road|LDV|Hybrid Electric",
        "ES|Transport|VKM|Pass|Road|LDV|Hybrid Liquids",
        "ES|Transport|VKM|Pass|Road|LDV|Liquids",
        "FE|Transport|Pass|Road|LDV|Liquids",
        "FE|Transport|Liquids|Hydrogen"
    ]

    data = data.loc[variables].reset_index()
    data = data.melt(id_vars=["Variable", "Scenario"], var_name="Year")

    fedf = data[data.Variable.str.startswith("FE|")]\
        .pivot(index=["Scenario", "Year"], columns="Variable", values="value")

    fedf["synshare"] = fedf["FE|Transport|Liquids|Hydrogen"]/fedf["FE|Transport|Pass|Road|LDV|Liquids"]
    fedf.loc[fedf.synshare > 1, "synshare"] = 1

    fedf.drop(columns=["FE|Transport|Liquids|Hydrogen", "FE|Transport|Pass|Road|LDV|Liquids"], inplace=True)
    fedf.reset_index(inplace=True)

    esdf = data[data.Variable.str.startswith("ES|")]
    esdf = esdf.merge(fedf, on=["Scenario", "Year"])
    esdf["Powertrain"] = esdf.Variable.str.replace("ES\|Transport\|VKM\|Pass\|Road\|LDV\|", "")

    esdf = esdf.drop(columns="Variable")
    syns = esdf[esdf.Powertrain.isin(["Hybrid Liquids", "Liquids"])]
    syns["value"] *= syns["synshare"]
    syns["Powertrain"] = syns.Powertrain.str.replace("Liquids", "Syn. Liquids")

    esdf.value = esdf.value.mask(esdf.Powertrain.isin(["Hybrid Liquids", "Liquids"]), esdf.value * (1-esdf.synshare))

    df = pd.concat([esdf, syns]).drop(columns="synshare")
    df["Powertrain"] = df.Powertrain.str.replace("Hybrid Electric", "Plug-in Hybrid")

    df["Scenario"] = df["Scenario"].str.replace("Budg900_", "")
    df.Year = df.Year.astype(int)
    df = df[(df.Year <= 2050) & (df.Year >= 2020)]

    df["demand"] = "default"
    df.loc[df.Scenario.str.contains("_LowD"), "demand"] = "low"
    df["Scenario"] = df.Scenario.str.replace("_LowD", "")
    toplot = df.pivot(index=["Scenario", "Year", "Powertrain"], columns="demand", values="value").reset_index()

    ord_scens = ["ElecPush", "H2Push", "ConvSyn", "Conv"]

    colors = {
        "Liquids": "#8c8c8c",
        "Syn. Liquids": "#a2a2a2",
        "Hybrid Liquids": "#ffc425",
        "Hybrid Syn. Liquids": "#fed873",
        "Plug-in Hybrid": "#f37735",
        "BEV": "#00b159",
        "FCEV": "#00aedb",
        "Gases": "#d11141"
    }
    ord_cols = [
        "Liquids", "Syn. Liquids", "Hybrid Liquids", "Hybrid Syn. Liquids",
        "Plug-in Hybrid", "BEV", "FCEV", "Gases"]

    nonLDplot = alt.Chart().transform_calculate(
        order=f"-indexof({ord_cols[::-1]}, datum.Powertrain)"
    ).mark_area().encode(
        x="Year:O",
        y=alt.Y("default:Q", title="LDV Demand [billion vkm]"),
        color=alt.Color(
            "Powertrain",
            scale=alt.Scale(
                domain=list(colors.keys()), range=list(colors.values())),
            legend=alt.Legend(orient='bottom')),
        order="order:Q"
    )
    LDplot = alt.Chart().mark_line(strokeDash=[2, 2]).encode(
        x="Year:O",
        y=alt.Y("sum(low):Q")
    )

    alt.layer(nonLDplot, LDplot, data=toplot).properties(width=150, height=250).facet(
        alt.Facet("Scenario", sort=ord_scens)
    ).save(
        os.path.join(
            plot_folder, "demand_by_drivetrain.png"),
        scale_factor=2)

def plot_fleet_composition_by_mode():
    # load fleet files
    data = pd.concat((
        pd.read_csv(
            os.path.join(remind_output_folder, "remind_{}.mif".format(sc)),
            sep=";", index_col=["Region", "Variable"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns=["Model", "Unit", "Unnamed: 24"])

    variables = [
        "ES|Transport|Pass|Road|LDV",
        "ES|Transport|Pass|Road|Bus",
        "ES|Transport|Pass|Road|Non-Motorized",
        "ES|Transport|Pass|Rail"
    ]

    data = data.loc[variables].reset_index()
    df = data.melt(id_vars=["Variable", "Scenario"], var_name="Year")

    df["Mode"] = df.Variable.str.replace("ES\|Transport\|Pass\|", "")
    df["Mode"] = df.Mode.str.replace("Road\|", "")

    df.drop(columns="Variable", inplace=True)

    df["Scenario"] = df["Scenario"].str.replace("Budg900_", "")
    df.Year = df.Year.astype(int)
    df = df[(df.Year < 2055) & (df.Year >= 2020)]

    # toplot = df.pivot(index=["Scenario", "Year", "Mode"], values="value").reset_index()

    ord_scens = ["ElecPush", "H2Push", "ConvSyn", "Conv",
                 "ElecPush_LowD", "H2Push_LowD", "ConvSyn_LowD", "Conv_LowD"]

    alt.Chart(df).mark_area().encode(
        x="Year:O",
        y=alt.Y("value:Q", title="Demand [billion pkm]"),
        color=alt.Color("Mode"),
    ).properties(width=150, height=150).facet(
        alt.Facet("Scenario", sort=ord_scens),
        columns=4
    ).save(
        os.path.join(
            plot_folder, "demand_mobility_modes.png"),
        scale_factor=2.0)


materials = ["Lithium", "Cobalt", "Platinum"]

def plot_fleet_materials():

    # available resources per capita
    # from Xu et al., supplement: Li 17 Mt, Co 7 Mt, Pt 0.66 Mt (Wittstock)
    # div by 7.8e9 (2020): Li 2.18 kg, Co 0.90 kg, Pt 0.08
    cons = {"Lithium": 2.18, "Cobalt": 0.9, "Platinum": 0.08}
    mat_map = {
        "Lithium": "ElecPush",
        "Cobalt": "ElecPush",
        "Platinum": "H2Push",
    }

    altplots = []
    for material, scen in mat_map.items():
        scens = [sc for sc in scenarios if scen in sc]

        data = pd.concat((
            pd.concat((
                pd.read_csv(
                    get_storage_file_path(sc, "materials", "high"),
                    header=0,
                    names=["Year", "Region", "Material", "value"],
                    index_col=["Region", "Material", "Year"])
                .loc[(region, material)]
                .assign(Scenario=sc)
                for sc in scens)).assign(Recycling="high"),
            pd.concat((
                pd.read_csv(
                    get_storage_file_path(sc, "materials", "low"),
                    header=0,
                    names=["Year", "Region", "Material", "value"],
                    index_col=["Region", "Material", "Year"])
                .loc[(region, material)]
                .assign(Scenario=sc)
                for sc in scens)).assign(Recycling="low")
        ))

        # per capita
        pop_data = get_pop_data().loc[2020]
        data["value"] /= pop_data["value"]*1e6
        data["Scenario"] = data["Scenario"].str.replace("Budg900_", "")

        data.index = pd.to_datetime(data.index, format="%Y")

        cum_data = data.groupby(["Scenario", "Recycling"]).resample("Y").mean()
        cum_data["value"] = cum_data["value"].interpolate()
        cum_data = cum_data.groupby(["Scenario", "Recycling"]).cumsum().reset_index()

        chart = alt.Chart(cum_data).mark_line().encode(
                x=alt.X('Year', title=""),
                y=alt.Y('value:Q',
                        axis=alt.Axis(
                            title="{} [kg per cap.]".format(material))),
                color='Scenario',
                strokeDash="Recycling")
        if material != "Platinum":
            chart += alt.Chart(pd.DataFrame({'y': [cons[material]]})).mark_rule(size=3, color="red").encode(y='y')
        altplots.append(chart.properties(width=150, height=150))

    alt.hconcat(*altplots)\
        .save(
            os.path.join(
                plot_folder, "materials_compared.png"),
            scale_factor=2.0)



def plot_endpoint_cumulative_total_stacked(year=2050):

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col=["Region"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios))

    units = {
        "Human health": "million DALYs",
        "Ecosystem quality": "thous. species years",
        "Resources": "trillion US$2000"
    }

    divs = {
        "Human health": 1e-6,
        "Ecosystem quality": 1e-3,
        "Resources": 1e-12
    }

    # fleet totals
    data["Category"] = data["Method"].apply(lambda x: eval(x)[1].capitalize())
    data["Method"] = data["Method"].apply(lambda x: eval(x)[2].capitalize())
    data = data.query("Method != 'Climate change'")
    data = data.groupby(["Year", "Category", "Method", "Scenario"]).sum().drop(columns="score_pkm")

    data.reset_index(inplace=True)
    data["Scenario"] = data["Scenario"].str.replace("Budg900_", "")
    data["Year"] = pd.to_datetime(data["Year"], format="%Y")
    data.set_index("Year", inplace=True)

    cum_data = data.groupby(["Category", "Method", "Scenario"]).resample("Y").mean()
    cum_data["total_score"] = cum_data["total_score"].interpolate()
    cum_data = cum_data.groupby(["Category", "Method", "Scenario"]).cumsum()

    ord_scens = ["Trend", "Synf_imp", "Elec_dom"]

    cats = ["Human health", "Ecosystem quality", "Resources"]
    cum_data = cum_data.loc[:, :, :, "2050-12-31"]
    cum_data.reset_index(inplace=True)

    cum_data.set_index("Category", inplace=True)

    charts = []
    method_order={
        "Human health": [
            'Climate change', 'Particulate matter formation', 'Human toxicity',
            'Photochemical oxidant formation', 'Ionising radiation', 'Ozone depletion',
        ],
        "Ecosystem quality": ['Climate change', 'Agricultural land occupation', 'Urban land occupation',
                        'Marine ecotoxicity', 'Terrestrial ecotoxicity', 'Freshwater eutrophication',
                        'Marine eutrophication', 'Terrestrial acidification'],
        "Resources": [
            "Fossil depletion", "Metal depletion"]
    }
    schemes = ["set2", "set3", "set1"]
    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        sel = cum_data.loc[cat]
        sorter = dict(zip(method_order[cat], range(len(method_order[cat]), 0, -1)))
        sel["Method_sorter"] = sel["Method"].map(sorter)
        sel["total_score"] *= divs[cat]
        charts.append(alt.Chart(sel).mark_bar().encode(

            x=alt.X('Scenario:N', title=None, sort=ord_scens),

            y=alt.Y('sum(total_score):Q',
                    axis=alt.Axis(
                        grid=False,
                        title=units[cat])),
            color=alt.Color('Method:N',
                            sort=method_order[cat],
                            scale={
                                "scheme": schemes[no]}),
            order="Method_sorter"
            ))

    alt.hconcat(*charts)\
        .configure_view(
            strokeOpacity=0)\
        .resolve_scale(
            color="independent")\
        .configure_legend(
            orient='bottom',
            columns=1).save(
                os.path.join(
                    plot_folder, "endpoint_cumulative_absolute_noclim.png"),
                scale_factor=2.0)


def plot_endpoint_fleet_stacked(years=[2020, 2030, 2045]):

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col=["Region"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios))

    units = {
        "Human health": "DALYs/a",
        "Ecosystem quality": "thous. species years",
        "Resources": "trillion US$2000"
    }

    divs = {
        "Human health": 1e-6,
        "Ecosystem quality": 1e-3,
        "Resources": 1e-12
    }

    # fleet totals
    data["Category"] = data["Method"].apply(lambda x: eval(x)[1].capitalize())
    data["Method"] = data["Method"].apply(lambda x: eval(x)[2].capitalize())

    data.replace({"Method": methodmap}, inplace=True)

    data = data.query("Method != 'Climate change'")
    data = data.groupby(["Year", "Category", "Method", "Scenario"]).sum().drop(
        columns=["score_pkm", "score_pkm_direct"])

    data.reset_index(inplace=True)
    data["score_upstream"] = data["total_score"] - data["total_score_direct"]

    ord_scens = ["Trend", "Synf_imp", "Elec_dom"]

    cats = ["Human health", "Ecosystem quality", "Resources"]

    data = data.set_index("Category").loc["Human health"].reset_index()
    
    data.drop(data[(data.Year == 2020) & (data.Scenario != "Trend")].index, inplace=True)
    data = data[data.Year.isin(years)]

    charts = []
    method_order=[
        'Feinstaub', 'Gifte',
        'PO Form.', 'Strahlung', 'Ozonschicht']

    cat = "Human health"
    schemes = ["set2", "set3", "set1"]

    sorter = dict(zip(method_order, range(len(method_order), 0, -1)))
    
    data["Method_sorter"] = data["Method"].map(sorter)

    data.to_csv(os.path.join(plot_folder, "endpoint_fleet_stacked.csv"))

    charts.append(alt.Chart(data).mark_bar().encode(
        x=alt.X('Scenario:N', title=None, sort=ord_scens),
        y=alt.Y('sum(total_score_direct):Q',
                axis=alt.Axis(
                    grid=False,
                    title=units[cat])),
        color=alt.Color('Method:N',
                        sort=method_order,
                        title="Kategorie",
                        scale={
                            "scheme": "set2"}),
        column=alt.Column('Year:N', title="direkte Wirkungen"),
        order="Method_sorter"
    ).resolve_scale(
                x="independent"
    ))
    charts.append(alt.Chart(data.sample(data.shape[0])).mark_bar().encode(
        x=alt.X('Scenario:N', title=None, sort=ord_scens),
        y=alt.Y('sum(score_upstream):Q',
                axis=alt.Axis(
                    grid=False,
                    title=units[cat])),
        color=alt.Color('Method:N',
                        sort=method_order,
                        title="Kategorie",
                        scale={
                            "scheme": "set2"}),
        column=alt.Column('Year:N', title="indirekte Wirkungen"),
        order="Method_sorter"
    ).resolve_scale(
                x="independent"
    ))

    alt.hconcat(*charts, title="Gesundheitseffekte durch die Gesamtflotte")\
        .configure_view(
            strokeOpacity=0)\
        .configure_legend(
            orient='bottom').save(
                os.path.join(
                    plot_folder, "endpoint_fleet_stacked.svg"),
                scale_factor=2.0)


def plot_climate_direct(year=2050):

    # 1990 EU Road Emissions: 716 MtCO2
    # 55% Target: 322 MtCO2

    # synfuel share will be substracted, we assume zero-carbon synfuels
    data = pd.concat((
        pd.read_csv(
            os.path.join(remind_output_folder, "remind_{}.mif".format(sc)),
            sep=";", index_col=["Region", "Variable"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns=["Model", "Unit", "Unnamed: 24"])

    variables = [
        "FE|Transport|Pass|Road|LDV|Liquids",
        "FE|Transport|Liquids|Hydrogen"
    ]

    data = data.loc[variables].reset_index()
    data = data.melt(id_vars=["Variable", "Scenario"], var_name="Year")
    data["Year"] = data.Year.astype(int)

    fedf = data.pivot(index=["Scenario", "Year"], columns="Variable", values="value")

    fedf["synshare"] = fedf["FE|Transport|Liquids|Hydrogen"]/fedf["FE|Transport|Pass|Road|LDV|Liquids"]
    fedf.loc[fedf.synshare > 1, "synshare"] = 1

    fedf.drop(columns=["FE|Transport|Liquids|Hydrogen", "FE|Transport|Pass|Road|LDV|Liquids"], inplace=True)

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col=["Region", "Method"])
        .loc[region, "('Custom Method', 'CO2 emissions', 'kgCO2')"]
        .assign(Scenario=sc)
        for sc in scenarios))

    # data.reset_index().set_index(["Year", "Variable", "Scenario"]).loc[2050]
    # fleet totals
    data = data.groupby(["Scenario", "Year"]).sum().drop(columns=["total_score", "score_pkm", "score_pkm_direct"])
    # substract synfuel share
    data["total_score_direct"] *= 1-fedf["synshare"]

    ord_scens = ["ElecPush", "H2Push", "ConvSyn", "Conv"]

    data.reset_index(inplace=True)
    data["Scenario"] = data["Scenario"].str.replace("Budg900_", "")
    data["Year"] = pd.to_datetime(data["Year"], format="%Y")

    data["demand"] = "default"
    data.loc[data.Scenario.str.contains("_LowD"), "demand"] = "low"
    data["Scenario"] = data.Scenario.str.replace("_LowD", "")

    data["total_score_direct"] *= 1e-9
    # data.Year = data.Year.astype(int)

    total = alt.Chart(data).mark_line().encode(
        x=alt.X('Year', title=""),
        y=alt.Y('total_score_direct:Q',
                axis=alt.Axis(
                    grid=False,
                    title="CO2 Emissions [MtCO2/a]")),
        color=alt.Color(
            'Scenario',
            sort=ord_scens,
            scale={"scheme": "dark2"},
            legend=alt.Legend(symbolStrokeWidth=4)),
        strokeDash=alt.StrokeDash("demand", title="Demand")
    ) + alt.Chart(pd.DataFrame({'y': [322]})).mark_rule(size=2, color="red").encode(y='y')

    total.properties(width=300, height=300)\
         .save(
             os.path.join(
                 plot_folder, "climate_direct.png"),
             scale_factor=2.0)


def plot_midpoint_tech_absolute_stacked(years = [2020, 2050]):
    results = []
    bw.projects.set_current(project_string(next(iter(scencars))))

    methods = [m for m in bw.methods if 'ReCiPe Endpoint (H) V1.13' == m[0]
               and "climate change" != m[2]
               and "natural land transformation" != m[2]]
    units = {
        "Human health": "DALYs/Mvkm",
        "Ecosystem quality": "species years lost/Mvkm",
        "Resources": "US$2000/Mvkm"
    }

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        for year in years:
            if year == 2020 and scenario == "Budg900_ConvSyn":
                continue
            if year == 2020:
                technames = {
                    "Budg900_H2Push": "FCEV",
                    "Budg900_Conv": "ICEV",
                    "Budg900_ElecPush": "BEV",
                }
            else:
                technames = scen_techs

            db = bw.Database(eidb_label(model, scenario, year))
            # find activity
            act = [a for a in db if
                   a["name"] == acstr
                   and a["location"] == region][0]
            for method in methods:
                lca = bw.LCA({act: 1}, method)
                lca.lci()
                lca.lcia()

                results.append(
                    (year, technames[scenario], method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["year", "technology", "method", "values"])

    df["category"] = df["method"].apply(lambda x: x[1].capitalize())
    df["method"] = df["method"].apply(lambda x: x[2].capitalize())
    df.set_index("category", inplace=True)

    df["values"] *= 1e6
    charts = []
    method_order={
        "Human health": [
            'Climate change', 'Particulate matter formation', 'Human toxicity',
            'Photochemical oxidant formation', 'Ionising radiation', 'Ozone depletion',
        ],
        "Ecosystem quality": ['Climate change', 'Agricultural land occupation', 'Urban land occupation',
                        'Marine ecotoxicity', 'Terrestrial ecotoxicity', 'Freshwater eutrophication',
                        'Marine eutrophication', 'Terrestrial acidification'],
        "Resources": [
            "Fossil depletion", "Metal depletion"]
    }
    schemes = ["set1", "set2", "set3"]
    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        charts.append(alt.Chart(df.loc[cat]).mark_bar().encode(

            # tell Altair which field to group columns on
            x=alt.X('technology:N', title=None),

            # tell Altair which field to use as Y values and how to calculate
            y=alt.Y('sum(values):Q',
                    axis=alt.Axis(
                        title=units[cat])),

            # tell Altair which field to use to use as the set of columns to be  represented in each group
            column=alt.Column('year:N', title=cat),

            # tell Altair which field to use for color segmentation
            color=alt.Color('method:N',
                            sort=method_order[cat],
                            scale={
                                "scheme": schemes[no]}),
            ).resolve_scale(
                x="independent"
            ))

    alt.hconcat(*charts)\
        .configure_view(
            # remove grid lines around column clusters
            strokeOpacity=0)\
        .resolve_scale(
            color="independent")\
        .configure_legend(
            orient='bottom',
            columns=1)\
        .save(os.path.join(plot_folder, "midpoint_tech_absolute_stacked_noclim.png"))


def plot_category_absolute_stacked(years = [2020, 2045]):
    bw.projects.set_current(project_string(next(iter(scencars))))

    methods = [m for m in bw.methods if 'ReCiPe Endpoint (H) V1.13' == m[0]
               and "climate change" != m[2]
               and "natural land transformation" != m[2]]

    selected_midpoints = {
        "Human health": [
            ('ReCiPe Midpoint (H) V1.13', 'human toxicity', 'HTPinf'),
            ('ReCiPe Midpoint (H) V1.13', 'particulate matter formation', 'PMFP'),
            ('ReCiPe Midpoint (H) V1.13', 'photochemical oxidant formation', 'POFP'),
        ],
        "Ecosystem quality": [
            ('ReCiPe Midpoint (H) V1.13', 'agricultural land occupation', 'ALOP'),
            ('ReCiPe Midpoint (H) V1.13', 'urban land occupation', 'ULOP'),
            ('ReCiPe Midpoint (H) V1.13', 'terrestrial ecotoxicity', 'TETPinf'),
        ],
        "Resources": [
            ('ReCiPe Midpoint (H) V1.13', 'fossil depletion', 'FDP'),
            ('ReCiPe Midpoint (H) V1.13', 'metal depletion', 'MDP'),
            ('ReCiPe Midpoint (H) V1.13', 'water depletion', 'WDP')
        ]
    }

    for cat in selected_midpoints:
        methods.extend(selected_midpoints[cat])
    endpoint_units = {
        "Human health": "1e-9 DALYs",
        "Ecosystem quality": "1e-9 species years lost",
        "Resources": "1e-3 US$2000"}
    midpoint_units = {
        ('ReCiPe Midpoint (H) V1.13', 'freshwater ecotoxicity', 'FETPinf'): "g 14DCB",
        ('ReCiPe Midpoint (H) V1.13', 'human toxicity', 'HTPinf'): "g 14DCB",
        ('ReCiPe Midpoint (H) V1.13', 'marine ecotoxicity', 'METPinf'): "g 14DCB",
        ('ReCiPe Midpoint (H) V1.13', 'terrestrial ecotoxicity', 'TETPinf'): "mg 14DCB",
        ('ReCiPe Midpoint (H) V1.13', 'metal depletion', 'MDP'): "g Fe",
        ('ReCiPe Midpoint (H) V1.13', 'agricultural land occupation', 'ALOP'): "m² yr",
        ('ReCiPe Midpoint (H) V1.13', 'fossil depletion', 'FDP'): "g oil",
        ('ReCiPe Midpoint (H) V1.13', 'freshwater eutrophication', 'FEP'): "mg P",
        ('ReCiPe Midpoint (H) V1.13', 'ionising radiation', 'IRP_HE'): "g U235",
        ('ReCiPe Midpoint (H) V1.13', 'marine eutrophication', 'MEP'): "mg N",
        ('ReCiPe Midpoint (H) V1.13', 'ozone depletion', 'ODPinf'): "µg CFC11",
        ('ReCiPe Midpoint (H) V1.13', 'particulate matter formation', 'PMFP'): "g PM10",
        ('ReCiPe Midpoint (H) V1.13', 'photochemical oxidant formation', 'POFP'): "g NMVOC",
        ('ReCiPe Midpoint (H) V1.13', 'terrestrial acidification', 'TAP100'): "mg SO2",
        ('ReCiPe Midpoint (H) V1.13', 'urban land occupation', 'ULOP'): "m² yr",
        ('ReCiPe Midpoint (H) V1.13', 'water depletion', 'WDP'): "l"}
    midpoint_multiplier = {
        ('ReCiPe Midpoint (H) V1.13', 'freshwater ecotoxicity', 'FETPinf'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'human toxicity', 'HTPinf'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'marine ecotoxicity', 'METPinf'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'terrestrial ecotoxicity', 'TETPinf'): 1e6,
        ('ReCiPe Midpoint (H) V1.13', 'metal depletion', 'MDP'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'agricultural land occupation', 'ALOP'): 1,
        ('ReCiPe Midpoint (H) V1.13', 'fossil depletion', 'FDP'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'freshwater eutrophication', 'FEP'): 1e6,
        ('ReCiPe Midpoint (H) V1.13', 'ionising radiation', 'IRP_HE'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'marine eutrophication', 'MEP'): 1e6,
        ('ReCiPe Midpoint (H) V1.13', 'ozone depletion', 'ODPinf'): 1e9,
        ('ReCiPe Midpoint (H) V1.13', 'particulate matter formation', 'PMFP'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'photochemical oxidant formation', 'POFP'): 1e3,
        ('ReCiPe Midpoint (H) V1.13', 'terrestrial acidification', 'TAP100'): 1e6,
        ('ReCiPe Midpoint (H) V1.13', 'urban land occupation', 'ULOP'): 1,
        ('ReCiPe Midpoint (H) V1.13', 'water depletion', 'WDP'): 1e3}
    endpoint_multiplier = {
        "Human health": 1e9,
        "Ecosystem quality": 1e9,
        "Resources": 1e3}

    results = []

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        for year in years:
            if year == 2020 and scenario == "Synf_imp":
                continue
            if year == 2020:
                technames = {
                    "Trend": "Otto",
                    "Elec_dom": "Elektro",
                }
            else:
                technames = scen_techs

            db = bw.Database(eidb_label(model, scenario, year))
            # find activity
            act = [a for a in db if
                   a["name"] == acstr
                   and a["location"] == region][0]
            for method in methods:
                lca = bw.LCA({act: 1}, method)
                lca.lci()
                lca.lcia()

                tot_score = lca.score
                proc_score = []

                for exc in act.technosphere():
                    if "tag" not in exc:
                        print("No tag found in {}".format(exc))
                    if exc.input["name"] == "polyethylene production, high density, granulate":
                        exc["tag"] = "energy storage"
                    lca.redo_lcia({exc.input: exc['amount']})
                    results.append(
                        (year, technames[scenario], method, exc["tag"],
                         exc.input["name"], lca.score))
                    proc_score.append(lca.score)

                results.append(
                    (year, technames[scenario], method, "direct", "driving emissions",
                     tot_score - sum(proc_score)))

    df = pd.DataFrame.from_records(
        results,
        columns=["year", "technology", "method", "tag", "activity", "values"])
    df.replace({"tag": rev_tags}, inplace=True)
    df.replace({"tag": trans}, inplace=True)

    df["values"][df["values"] < 0.] = 0.
    df["method_type"] = df["method"].apply(lambda x: x[0])
    df.set_index("method_type", inplace=True)

    method_order={
        "Human health": [
            'Climate change', 'Particulate matter formation', 'Human toxicity',
            'Photochemical oxidant formation', 'Ionising radiation', 'Ozone depletion',
        ],
        "Ecosystem quality": ['Climate change', 'Agricultural land occupation', 'Urban land occupation',
                        'Marine ecotoxicity', 'Terrestrial ecotoxicity', 'Freshwater eutrophication',
                        'Marine eutrophication', 'Terrestrial acidification'],
        "Resources": [
            "Fossil depletion", "Metal depletion"]
    }

    legend_cn = [1, 1, 2]

    # prepare endpoint data
    ep_str = 'ReCiPe Endpoint (H) V1.13'
    epdf = df.loc[ep_str]
    epdf = epdf.groupby(["year", "technology", "method"]).sum().reset_index()
    epdf["category"] = epdf["method"].apply(lambda x: x[1].capitalize())
    epdf["method_name"] = epdf["method"].apply(lambda x: x[2].capitalize())
    epdf.replace({"method_name": methodmap}, inplace=True)
    epdf.set_index("category", inplace=True)

    # prepare midpoint data
    mp_str = 'ReCiPe Midpoint (H) V1.13'
    mpdf = df.loc[mp_str]

    mpdf["method_name"] = mpdf["method"].apply(
        lambda x: x[1].capitalize())
    mpdf.replace({"method_name": methodmap}, inplace=True)
    mpdf["unit"] = mpdf["method"].apply(lambda x: " [{}]".format(midpoint_units[x]))
    mpdf["multiplier"] = mpdf["method"].apply(lambda x: midpoint_multiplier[x])

    mpdf["method_label"] = mpdf["method_name"] + mpdf["unit"]
    mpdf["values"] *= mpdf["multiplier"]

    mpdf.set_index("method", inplace=True)

    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        # Endpoints

        eps = epdf.loc[cat]
        eps["values"] *= endpoint_multiplier[cat]
        charts = []
        eps.to_csv(os.path.join(plot_folder, "{}_endpoints.csv".format(cat)))

        charts.append(alt.Chart(eps).mark_bar().encode(

            # tell Altair which field to group columns on
            x=alt.X('technology:N', title=None),

            # tell Altair which field to use as Y values and how to calculate
            y=alt.Y('sum(values):Q', title=None),

            # tell Altair which field to use to use as the set of columns to be  represented in each group
            column=alt.Column('year:N', title="Effekte [{}]".format(endpoint_units[cat])),

            # tell Altair which field to use for color segmentation
            color=alt.Color('method_name:N',
                            sort=method_order[cat],
                            title="Wirkkategorie",
                            legend=alt.Legend(),
                            scale={"scheme": "set2"}
            )).resolve_scale(
                x="independent"
            ))

        mps = mpdf.loc[selected_midpoints[cat]].query("year == 2045")
        mps.to_csv(os.path.join(plot_folder, "{}_midpoints.csv".format(cat)))

        charts.append(alt.Chart(mps).mark_bar().encode(

            # tell Altair which field to group columns on
            x=alt.X('technology:N', title=None),

            # tell Altair which field to use as Y values and how to calculate
            y=alt.Y('sum(values):Q', title=None),

            # tell Altair which field to use to use as the set of columns to be  represented in each group
            column=alt.Column('method_label:N', title="Schadstoffe (2045)"),

            # tell Altair which field to use for color segmentation
            color=alt.Color('tag:N', legend=alt.Legend(title="Prozesskategorie"),
                            scale={"scheme": "set1"})
        ).resolve_scale(y="independent"))

        alt.hconcat(*charts, title="{}, pro Fahrzeug-Km".format(trans[cat]))\
           .configure_view(
               # remove grid lines around column clusters
               strokeOpacity=0)\
           .resolve_scale(
               color="independent")\
           .save(os.path.join(plot_folder, "{}_combined.svg".format(cat)),
                 scale_factor=2.0)


def plot_climate(years = [2020, 2050]):
    results = []

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        for year in years:
            if year == 2020 and scenario == "Budg900_ConvSyn":
                continue
            if year == 2020:
                technames = {
                    "Budg900_H2Push": "FCEV",
                    "Budg900_Conv": "ICEV",
                    "Budg900_ElecPush": "BEV",
                }
            else:
                technames = scen_techs
            db = bw.Database(eidb_label(model, scenario, year))
            # find activity
            act = [a for a in db if
                   a["name"] == acstr
                   and a["location"] == region]
            assert len(act) == 1, "Found too many activities for {} in {}".format(year, scenario)
            act = act[0]
            lca = bw.LCA({act: 1}, climate_method)
            lca.lci()
            lca.lcia()

            tot_score = lca.score
            proc_score = []

            for exc in act.technosphere():
                if "tag" not in exc:
                    print("No tag found in {}, {}".format(exc, db))
                if exc.input["name"] == "polyethylene production, high density, granulate":
                    exc["tag"] = "energy storage"
                lca.redo_lcia({exc.input: exc['amount']})
                results.append(
                    (year, technames[scenario], exc["tag"],
                     exc.input["name"], lca.score))
                proc_score.append(lca.score)

            results.append(
                (year, technames[scenario], "direct", "driving emissions",
                 tot_score - sum(proc_score)))

    df = pd.DataFrame.from_records(
        results,
        columns=["year", "scenario", "tag", "activity", "values"])
    df.reset_index(inplace=True)
    df.replace({"tag": rev_tags}, inplace=True)

    df = df.groupby(["year", "scenario", "tag"]).sum().drop(columns="index").reset_index()

    # df["values"] *= 1e6
    techs = alt.Chart(df).mark_bar().encode(
        x=alt.X('scenario:N', title=None),
        y=alt.Y('sum(values):Q',
                axis=alt.Axis(
                    title="GWP 100a [kg CO2-eq./km]")),
        column=alt.Column('year:Q', title=""),
        color=alt.Color(
            'tag:N',
            scale={"scheme": "set1"},
            legend=alt.Legend(orient="right", title="Process category")))\
        .resolve_scale(x="independent")\
        .properties(width=100, height=250)

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col=["Region", "Method"])
        .loc[region, str(climate_method)]
        .assign(Scenario=sc)
        for sc in scenarios))

    # data.reset_index().set_index(["Year", "Variable", "Scenario"]).loc[2050]
    # fleet totals
    data = data.groupby(["Year", "Scenario"]).sum().drop(columns="score_pkm")

    data.reset_index(inplace=True)
    data["Scenario"] = data["Scenario"].str.replace("Budg900_", "")
    data["Year"] = pd.to_datetime(data["Year"], format="%Y")
    data.set_index("Year", inplace=True)

    cum_data = data.groupby("Scenario").resample("Y").mean().interpolate()

    cum_data = cum_data.groupby("Scenario").cumsum()

    ord_scens = ["ElecPush", "H2Push", "ConvSyn", "Conv"][::-1]

    cum_data = cum_data.reset_index()# .loc["2050-12-31"]

    cum_data["total_score"] *= 1e-12

    cum_data["demand"] = "default"
    cum_data.loc[cum_data.Scenario.str.contains("_LowD"), "demand"] = "low"
    cum_data["Scenario"] = cum_data.Scenario.str.replace("_LowD", "")

    total = alt.Chart(cum_data).mark_line(size=3).encode(
        x=alt.X('Year', title=""),
        y=alt.Y('total_score:Q',
                axis=alt.Axis(
                    title="GWP 100a [GtCO2-eq]",
                    tickCount=10)),
        color=alt.Color(
            'Scenario',
            sort=ord_scens,
            scale={"scheme": "dark2"},
            legend=alt.Legend(symbolStrokeWidth=4)),
        strokeDash=alt.StrokeDash("demand", title="Demand")
    ).properties(width=300, height=250)

    alt.concat(total, techs)\
       .resolve_scale(color="independent", strokeDash="independent")\
       .save(
           os.path.join(
               plot_folder, "climate.png"),
           scale_factor=2.0)


def plot_midpoint_tech_relative(year = 2050):
    results = []
    # reference activity
    ref_scenario = "Budg900_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))
    methods = [m for m in bw.methods if 'ReCiPe Endpoint (H) V1.13' == m[0]
               and "climate change" != m[2]
               and "natural land transformation" != m[2]]

    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"] == scencars[ref_scenario]
        and a["location"] == region][0]

    for method in methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        results.append(
            (ref_string, method, lca.score))

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"] == acstr
               and a["location"] == region][0]
        for method in methods:
            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            results.append(
                (scen_techs[scenario], method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["technology", "method", "values"])

    df["category"] = df["method"].apply(lambda x: x[1].capitalize())
    df["method"] = df["method"].apply(lambda x: x[2].capitalize())

    df.set_index(["technology", "category", "method"], inplace=True)

    df = df.div(df.loc[ref_string], axis="index")
    df.reset_index(["technology"], inplace=True)

    fig, ax = plt.subplots(
        1, 3, sharey=True,
        figsize=(8, 5.5), gridspec_kw={'width_ratios': [0.6, 0.9, 0.2]})
    # fig.suptitle("Impacts compared to an average car in 2020")

    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        dfwide = df.loc[cat].pivot(columns="technology", values="values")\
                            .drop(columns=ref_string) * 100

        dfwide.plot.bar(ax=ax[no], width=0.8, legend=False)
        ax[no].axhline(100.)
        ax[no].set_xlabel("")
        ax[no].set_ylabel("rel. impact (%)")
        ax[no].yaxis.set_tick_params(length=0)
        ax[no].title.set_text(cat)
        # ax[no].set_xticklabels(
        #     ax[no].get_xticklabels(),
        #     rotation=45, ha="right")


    ax[0].yaxis.set_tick_params(length=2)
    handles, labels = ax[-1].get_legend_handles_labels()

    fig.legend(handles[:4], labels[:4], loc='lower left', bbox_to_anchor= (0.1, 0.75))
    fig.tight_layout()
    fname = ("midpoint_tech_relative.png")
    fig.savefig(
        os.path.join(
            plot_folder, fname))


def plot_contribution_analysis_stackedbars_relative(year = 2050):

    results = []
    # reference activity
    ref_scenario = "Budg900_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))
    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"] == scencars[ref_scenario]
        and a["location"] == region][0]

    for method in selected_methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        tot_score = lca.score
        proc_score = []

        for exc in act.technosphere():
            if exc.input["name"] == "polyethylene production, high density, granulate":
                exc["tag"] = "energy storage"
            lca.redo_lcia({exc.input: exc['amount']})
            results.append(
                (ref_string, method, exc["tag"],
                 exc.input["name"], lca.score))
            proc_score.append(lca.score)

        results.append(
            (ref_string, method, "direct", "driving emissions",
             tot_score - sum(proc_score)))


    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"] == acstr
               and a["location"] == region][0]
        for method in selected_methods:

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            tot_score = lca.score
            proc_score = []

            for exc in act.technosphere():
                if exc.input["name"] == "polyethylene production, high density, granulate":
                    exc["tag"] = "energy storage"
                lca.redo_lcia({exc.input: exc['amount']})
                results.append(
                    (scen_techs[scenario], method, exc["tag"],
                     exc.input["name"], lca.score))
                proc_score.append(lca.score)

            results.append(
                (scen_techs[scenario], method, "direct", "driving emissions",
                 tot_score - sum(proc_score)))

    df = pd.DataFrame.from_records(
        results,
        columns=["scenario", "method", "tag", "activity", "values"])
    df.reset_index(inplace=True)
    df["method"] = df["method"].apply(lambda x: "{}, {}".format(
        x[1].capitalize(), x[2]))

    df.to_csv("output/scenario_comparison_tagged.csv")
    ## plot in R

    # df = df.groupby(["scenario", "method", "tag"]).sum().drop(columns="index")
    # norm = df.loc[ref_string].groupby("method").sum()
    # df /= norm
    # df = df.loc[tech_order].reset_index()
    # alt.Chart(df).mark_bar().encode(
    #     x=alt.X('scenario:N', sort=tech_order),
    #     y=alt.Y('sum(values):Q',
    #             axis=alt.Axis(
    #                 grid=False)),
    #     color=alt.Color('tag:N'),
    #     column=alt.Column('method:N',
    #                       header=alt.Header(labelAngle=60)))


def plot_selected_stackedbars_absolute(year = 2050):

    results = []
    # reference activity

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"] == acstr
               and a["location"] == region][0]
        for method in selected_methods:

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            tot_score = lca.score
            proc_score = []

            for exc in act.technosphere():
                if exc.input["name"] == "polyethylene production, high density, granulate":
                    exc["tag"] = "energy storage"
                lca.redo_lcia({exc.input: exc['amount']})
                results.append(
                    (scen_techs[scenario], method, exc["tag"],
                     exc.input["name"], lca.score))
                proc_score.append(lca.score)

            results.append(
                (scen_techs[scenario], method, "direct", "driving emissions",
                 tot_score - sum(proc_score)))

    df = pd.DataFrame.from_records(
        results,
        columns=["scenario", "method", "tag", "activity", "values"])
    df.reset_index(inplace=True)
    df["category"] = df["method"].apply(lambda x: x[1].capitalize())
    df["method"] = df["method"].apply(lambda x: x[2].capitalize())

    methodmap = {
        "Photochemical oxidant formation": "PO formation",
        "Particulate matter formation": "PM formation",
        'Urban land occupation': "Urban land",
        'Agricultural land occupation': "Agricultural land"
    }
    df.replace({"method": methodmap}, inplace=True)
    df.set_index("category", inplace=True)

    df["values"] *= 1e9
    method_order={
        "Human health": [
            'Human toxicity', 'Particulate matter formation',
            'Photochemical oxidant formation'
        ],
        "Ecosystem quality": ['Agricultural land occupation', 'Urban land occupation'],
        "Resources": ["Metal depletion"]
    }

    units = {
        "Human health": "DALYs/Gvkm",
        "Ecosystem quality": "species years lost/Gvkm",
        "Resources": "US$2000/Gvkm"
    }

    charts = []
    domains = {
        "Human health": [0, 90],
        "Ecosystem quality": [0, 0.5],
        "Resources": [0, 9e6]
    }
    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        charts.append(alt.Chart(df.loc[cat]).mark_bar().encode(

            # tell Altair which field to group columns on
            y=alt.Y('scenario:N', title=None, sort=tech_order),

            # tell Altair which field to use as Y values and how to calculate
            x=alt.X('sum(values):Q',
                    axis=alt.Axis(
                        title=units[cat]),
                    scale=alt.Scale(domain=domains[cat])),

            # tell Altair which field to use to use as the set of columns to be  represented in each group
            row=alt.Row('method:N', title=cat,
                              sort=method_order[cat]),

            # tell Altair which field to use for color segmentation
            color=alt.Color('tag:N')))

    alt.vconcat(*charts)\
        .configure_view(
            # remove grid lines around column clusters
            strokeOpacity=0)\
        .save(os.path.join(plot_folder, "selected_midpoint_stacked.png"))

if __name__ == '__main__':
    # print("plot_midpoint_tech_relative(2050)")
    # plot_midpoint_tech_relative(2050)
    # print("plot_climate()")
    # plot_climate()
    # print("plot_endpoint_cumulative_total_stacked()")
    # plot_endpoint_cumulative_total_stacked()
    # print("plot_midpoint_tech_absolute_stacked()")
    # plot_midpoint_tech_absolute_stacked()
    # print("plot_selected_stackedbars_absolute()")
    # plot_selected_stackedbars_absolute()
    print("plot_category_absolute_stacked()")
    plot_category_absolute_stacked()
    # print("plot_endpoint_fleet_stacked()")
    # plot_endpoint_fleet_stacked()
    # print("plot_fleet_materials()")
    # plot_fleet_materials()
    # print("plot_fleet_composition")
    # plot_fleet_composition()
    # print("plot_fleet_composition_by_drivetrain")
    # plot_fleet_composition_by_drivetrain()
    # print("plot_fleet_composition_by_mode")
    # plot_fleet_composition_by_mode()
