#!/bin/bash
env="lca-env";
conda create -n $env python=3.9;
conda activate $env;
conda install -y -q -c conda-forge -c cmutel -c haasad -c konstantinstadler brightway2 wurst;
conda install conda-pack altair altair_viewer altair_saver;


