#!/bin/bash

folder=~/git/ariadne-lca/runs
runs=$(cat ${folder}/runs.txt)

for run in $runs;do
    scen=${run##*/}
    scen=${scen%_*_*}
    scp cluster:$run/REMIND_generic_${scen}_withoutPlus.mif $folder/remind_$scen.mif
    # scp cluster:$run/vintcomp.csv $folder/${scen}_vintcomp.csv
done
