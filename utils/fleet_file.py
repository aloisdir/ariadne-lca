import pandas as pd
import numpy as np
import xarray as xr
import pycountry
import pickle
import os

def fleet_file(files):
    # Load diesel shares from IEA on OECD countries
    share_diesel = pd.read_csv(
        "diesel_share_oecd.csv", sep=";", usecols=range(1, 16), index_col=[0]
    )
    share_diesel.columns = [int(c) for c in share_diesel.columns]
    shares = pd.concat(
        [
            share_diesel,
            pd.DataFrame(columns=range(2019, 2051)),
            pd.DataFrame(columns=range(2000, 2005)),
        ]
    )
    shares = shares.T.fillna(method="ffill")
    shares = shares.fillna(method="bfill")

    for file in files:

        print("Working on {}".format(file))
        # Read the fleet composition CSV file
        df = pd.read_csv(file, delimiter=",", names=[
            "year", "region", "vehicle_type", "technology",
            "variable", "full_demand_vkm", "vintage_demand_vkm"],
                         skiprows=5)
        df = df.fillna(0)

        model="remind"


        df["IAM_region"] = df["region"]


        # Dictionary map of shape {`REMIND car size`:`carculator car size`}
        d_map_sizes = {
            'Truck (0-1t)':'3.5t',
            'Truck (0-3.5t)':'3.5t',
            'Truck (0-4.5t)':'3.5t',
            'Truck (1-6t)':'7.5t',
            'Truck (16-32t)':'26t',
            'Truck (3.5-16t)':'26t',
            'Truck (4.5-15t)':'7.5t',
            'Truck (6-15t)':'7.5t',
            'Truck (6-30t)':'18t',
            'Truck (>15t)':'26t',
            'Truck (>32t)':'40t',
            'Truck (0-2t)':'3.5t',
            'Truck (0-6t)':'3.5t',
            'Truck (2-5t)':'3.5t',
            'Truck (5-9t)':'7.5t',
            'Truck (6-14t)':'7.5t',
            'Truck (9-16t)':'18t',
            'Truck (>14t)':'18t',
            'Truck (0-2.7t)':'3.5t',
            'Truck (2.7-4.5t)':'3.5t',
            'Truck (4.5-12t)':'7.5t',
            'Truck (>12t)':'18t',
            'Compact Car': 'Lower medium',
            'Large Car and SUV': "Large",
            'Mini Car': 'Mini',
            'Subcompact Car': 'Small',
            'Van': 'Van',
            'Midsize Car': 'Medium',
            'Light Truck and SUV': 'SUV',
            'Large Car': 'Large',
            'Multipurpose Vehicle': 'SUV'

        }

        # Dictionary map of shape {`REMIND car powertrain`:`carculator car powertrain`}
        d_map_tech = {
            "BEV": "BEV",
            "FCEV": "FCEV",
            "Hybrid Electric": "PHEV",
            "Hybrid Liquids": "HEV",
            "Liquids": "ICEV",
            "NG": "ICEV-g",
        }



        df["powertrain"] = df["technology"].map(d_map_tech)
        df["size"] = df["vehicle_type"].map(d_map_sizes)

        # Clean up
        df["variable"] = df["variable"].str.replace(r"\D", "").astype(int)

        # Rename `variable`, since it is a built-in name used by `xarray`
        df = df.rename(columns={"variable": "vintage_year"})

        df = df.loc[~df["size"].isnull()]

        def get_diesel_factor(row):

            if row["IAM_region"] in shares.columns:
                if row["vintage_year"] <= 2050:
                    return float(shares.loc[row["vintage_year"], row["IAM_region"]])
                else:
                    return float(shares.loc[2050, row["IAM_region"]])
            else:
                return 0

        df["diesel_factor"] = df.apply(get_diesel_factor, axis=1)

        new_df_p = df.loc[df["powertrain"].isin(("HEV", "PHEV", "ICEV"))]
        new_df_p.loc[:, "powertrain"] += "-p"
        new_df_p.loc[:, "vintage_demand_vkm"] *= 1 - new_df_p.loc[:, "diesel_factor"]

        new_df_d = df.loc[df["powertrain"].isin(("HEV", "PHEV", "ICEV"))]
        new_df_d.loc[:, "powertrain"] += "-d"
        new_df_d.loc[:, "vintage_demand_vkm"] *= new_df_d.loc[:, "diesel_factor"]

        for r in df["IAM_region"].unique():
            for y in df["year"].unique():
                for s in df["size"].unique():
                    for p in ["HEV", "PHEV", "ICEV"]:

                        total_vintage_vkm_p = new_df_p.loc[(new_df_p["IAM_region"]==r)&(new_df_p["year"]==y)
                                                           &(new_df_p["size"]==s)&(new_df_p["powertrain"]==p + "-p"), "vintage_demand_vkm"].sum()

                        total_vintage_vkm_d = new_df_d.loc[(new_df_d["IAM_region"]==r)&(new_df_d["year"]==y)
                                                           &(new_df_d["size"]==s)&(new_df_d["powertrain"]==p + "-d"), "vintage_demand_vkm"].sum()

                        if total_vintage_vkm_p / (total_vintage_vkm_p + total_vintage_vkm_d) > 0:
                            new_df_p.loc[(new_df_p["IAM_region"]==r)&(new_df_p["year"]==y)
                                         &(new_df_p["size"]==s)&(new_df_p["powertrain"]==p + "-p"), "full_demand_vkm"] *=\
                                             total_vintage_vkm_p / (total_vintage_vkm_p + total_vintage_vkm_d)

                            # print(total_vintage_vkm_p / (total_vintage_vkm_p + total_vintage_vkm_d))

                        if total_vintage_vkm_d / (total_vintage_vkm_p + total_vintage_vkm_d) > 0:
                            new_df_d.loc[(new_df_d["IAM_region"]==r)&(new_df_d["year"]==y)
                                         &(new_df_d["size"]==s)&(new_df_d["powertrain"]==p + "-d"), "full_demand_vkm"] *=\
                                             total_vintage_vkm_d / (total_vintage_vkm_p + total_vintage_vkm_d)

                            # print(total_vintage_vkm_d / (total_vintage_vkm_p + total_vintage_vkm_d))


        df = df.loc[~df["powertrain"].isin(("HEV", "PHEV", "ICEV"))]
        df = pd.concat([df, new_df_p, new_df_d])

        print("done")
        # Distribute the transport demand of 2010 to anterior years

        distr_km = {
            # EURO 4 - 60%
            2010: 0.15,
            2009: 0.15,
            2008: 0.10,
            2007: 0.10,
            2006: 0.10,
            # EURO 3 - 25%
            2005: 0.05,
            2004: 0.05,
            2003: 0.05,
            2002: 0.05,
            2001: 0.05,
            # EURO 2 - 10%
            2000: 0.025,
            1999: 0.025,
            1998: 0.025,
            1997: 0.025,
            # EURO 1 - 5%
            1996: 0.05,
        }

        new_df = pd.DataFrame()
        for y in range(2010, 1995, -1):
            temp_df = df.loc[df["vintage_year"] == 2010].copy(deep=True)
            temp_df["vintage_year"] = y
            temp_df["vintage_demand_vkm"] *= distr_km[y]
            new_df = pd.concat([new_df, temp_df])

        df = df.loc[df["vintage_year"] != 2010]

        df = pd.concat([df, new_df])

        df = df[["IAM_region", "size", "powertrain", "year", "vintage_year", "full_demand_vkm", "vintage_demand_vkm"]]

        df = df.groupby(["IAM_region", "size", "powertrain", "year", "vintage_year"]).sum().reset_index()

        df = df[df["year"]<=2060]

        list_res = []
        for r in df["IAM_region"].unique():
            # print(r)
            for y in df["year"].unique():
                # print(y)
                for p in df["powertrain"].unique():
                    # print(p)
                    for s in df["size"].unique():
                        sum_vkm = df.loc[(df["IAM_region"]==r)&(df["year"]==y)&(df["powertrain"]==p)&(df["size"]==s),
                                         "vintage_demand_vkm"].sum()
                        full_demand_vkm = df.loc[(df["IAM_region"]==r)&(df["year"]==y)&(df["powertrain"]==p)&(df["size"]==s),
                                                 "full_demand_vkm"].mean()
                        # IAM_region, full_demand_vkm, powertrain, size, vintage_demand_vkm, vintage_year, year
                        list_res.append(
                            [
                                r, s, p, y, y, full_demand_vkm, full_demand_vkm-sum_vkm
                            ]
                        )

        new_df = pd.DataFrame(list_res, columns=["IAM_region", "size", "powertrain", "year", "vintage_year", "full_demand_vkm", "vintage_demand_vkm"])

        df = pd.concat([df, new_df])

        df = df.fillna(0)

        df["vintage_demand_vkm"] = df["vintage_demand_vkm"].astype("int")

        df = df.loc[df["vintage_demand_vkm"]>0]
        fname, ext = os.path.splitext(file)
        df[["year", "IAM_region", "powertrain", "size", "vintage_year", "vintage_demand_vkm"]].to_csv(
            fname + "_transformed" + ext)

import glob
flist = glob.glob("/home/alois/git/ariadne-lca/runs/*_vintcomp.csv")
fleet_file(flist)


