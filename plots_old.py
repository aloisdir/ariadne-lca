from premise.utils import eidb_label
from lca2rmnd.utils import project_string
from lcatool import scenarios, years, mifs
from lcatool import get_storage_file_path

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import plotly.express as px
import pandas as pd
import numpy as np
import seaborn as sns
import altair as alt
import brightway2 as bw
from cycler import cycler
import os
import bw2analyzer as bw2a

alt.renderers.enable('altair_viewer')

plt.style.use("ggplot")
plt.rcParams.update({'font.size': 10})
SMALL_SIZE = MEDIUM_SIZE = BIGGER_SIZE = 10
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title    fig.tight_layout()

plot_folder = "plots/"
region = "EUR"
# size = "Medium"
model = "remind"

ep_categories = ["human health", "ecosystem quality", "resources"]

units = {
    "human health": "DALYs",
    "ecosystem quality": "Species Loss",
    "resources": "US$2000"
}

# population data
pop_data = pd.read_csv(mifs[0], sep=";").drop(columns=["Model", "Scenario"])
if(len(pop_data.columns == 20)):
    pop_data.drop(columns=pop_data.columns[-1], inplace=True)
pop_data = pop_data[
    (pop_data["Variable"] == "Population") &
    (pop_data["Region"] == "EUR")].drop(columns=["Unit", "Region", "Variable"])
pop_data = pop_data.melt(var_name="Year").astype({"Year": int})
pop_data = pop_data[pop_data["Year"].isin(years)]
pop_data.set_index("Year", inplace=True)

scen_techs = {
    "Budg1100_H2Push": "FCEV (H2Push)",
    "Budg1100_Conv": "ICEV-p (Conv)",
    "Budg1100_ElecPush": "BEV (ElecPush)",
    "Budg1100_ConvSyn": "ICEV-p (ConvSyn)",
}

tech_order = [
    "BEV (ElecPush)",
    "FCEV (H2Push)",
    "ICEV-p (ConvSyn)",
    "ICEV-p (Conv)",
]

scencars = {
    "Budg1100_H2Push": "transport, passenger car, fuel cell electric, Medium",
    "Budg1100_Conv": "transport, passenger car, gasoline, Medium",
    "Budg1100_ElecPush": "transport, passenger car, battery electric, Medium",
    "Budg1100_ConvSyn": "transport, passenger car, gasoline, Medium",
}


selected_methods = [
    ('ReCiPe Endpoint (H) V1.13', 'human health', 'human toxicity'),
    ('ReCiPe Endpoint (H) V1.13', 'ecosystem quality', 'marine ecotoxicity'),
    ('ReCiPe Endpoint (H) V1.13', 'ecosystem quality', 'terrestrial ecotoxicity'),
    ('ReCiPe Endpoint (H) V1.13', 'ecosystem quality', 'agricultural land occupation'),
    ('ReCiPe Endpoint (H) V1.13', 'ecosystem quality', 'urban land occupation'),
    ('ReCiPe Endpoint (H) V1.13', 'resources', 'metal depletion'),
]


def plot_midpoint_scen_total_norm_bar(years=[2030, 2050]):
    # Midpoint impacts normalized to Base

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col=["Year", "Region"])
        .loc[[(year, region) for year in years]]
        .assign(Scenario=sc)
        for sc in scenarios))

    # fleet totals
    data = data.groupby(["Year", "Scenario", "Method"]).sum()

    # normalize
    # data = (data / data.loc["Budg1100_Conv"] - 1)
    data.reset_index(inplace=True)

    data["Endpoint"] = data["Method"].apply(lambda x: eval(x)[1])
    data["Category"] = data["Method"].apply(lambda x: eval(x)[2])

    data = data.merge(conv_df)
    data["ep_score"] = data["total_score"] * data["factor"]

    fig, ax = plt.subplots(3, 1)

    for idx, cat in enumerate(ep_categories):
        toplot = data[(data.Endpoint == cat) & (data.Category != "total")]
        toplot.set_index("Category", inplace=True)

        vardt = toplot[toplot.Year == years[0]]
        vardt = vardt.pivot(columns="Scenario", values="ep_score")

        vardt.plot.barh(
            ax=ax[idx],
            width=1,
            title=("Endpoint Impacts, {}"
                   .format(cat)))

        vardt = toplot[toplot.Year == years[1]]
        vardt = vardt.pivot(columns="Scenario", values="total_score")

        vardt.plot.barh(
            ax=ax[idx]
        )

    plt.xscale("symlog")
    plt.axvline(0)
    plt.xticks([-0.5, -0.25, 0, 0.25, 0.5, 1, 2],
               [50, 75, 100, 125, 150, 200, 300])
    plt.xlabel("%")
    plt.tight_layout()
    fname = ("scenario_performance_midpoint_fullfleet_{}_bar.pdf"
             .format(year))
    plt.savefig(
        os.path.join(
            plot_folder, fname))


def plot_endpoint_cumulative_total_bar(year=2050):

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col=["Region"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios))

    units = {
        "Human health": "million DALYs",
        "Ecosystem quality": "thous. species years",
        "Resources": "trillion US$2000"
    }

    divs = {
        "Human health": 1e-6,
        "Ecosystem quality": 1e-3,
        "Resources": 1e-12
    }

    # fleet totals
    data["Category"] = data["Method"].apply(lambda x: eval(x)[1].capitalize())
    data = data.groupby(["Year", "Category", "Scenario"]).sum().drop(columns="score_pkm")

    data.reset_index(inplace=True)
    data["Scenario"] = data["Scenario"].str.replace("Budg1100_", "")
    data["Year"] = pd.to_datetime(data["Year"], format="%Y")
    data.set_index("Year", inplace=True)

    cum_data = data.groupby(["Category", "Scenario"]).resample("Y").mean()
    cum_data["total_score"] = cum_data["total_score"].interpolate()
    cum_data = cum_data.groupby(["Category", "Scenario"]).cumsum()

    conv_scens = ["ElecPush", "Conv", "H2Push", "ConvSyn"]
    lowd_scens = ["ElecPush_LowD", "Conv_LowD"]

    cats = ["Human health", "Ecosystem quality", "Resources"]
    cum_data = cum_data.loc[:, :, "2050-12-31"]

    fig, ax = plt.subplots(1, 3, figsize=(9, 4))
    for nx, cat in enumerate(cats):
        dt = cum_data.loc[cat] * divs[cat]
        selec = dt.loc[lowd_scens].T
        selec["ConvSyn_LowD"] = 0
        selec["H2Push_LowD"] = 0
        selec[[sc + "_LowD" for sc in conv_scens]].plot.bar(ax=ax[nx], legend=False)
        selec = dt.T
        selec[conv_scens].plot.bar(ax=ax[nx], legend=False, alpha=0.5)
        ax[nx].set_ylabel("{}".format(units[cat]))
        ax[nx].xaxis.set_ticklabels([])
        ax[nx].title.set_text(cat)

    handles, labels = ax[2].get_legend_handles_labels()
    handles = [handles[0], handles[4], handles[1], handles[5]] + handles[6:]
    labels = ["ElecPush (lowD)", "ElecPush", "Conv (lowD)", "Conv", "H2Push", "ConvSyn"]

    ax[2].legend(handles, labels, loc='lower left', bbox_to_anchor= (1., 0.7), ncol=1,
            borderaxespad=0, frameon=False)

    # fig.suptitle("Impacts compared to an average car in 2020")
    # fig.subplots_adjust(right=0.85, wspace=0.55)
    fig.tight_layout()
    fname = ("endpoint_cumulative_absolute.png")
    fig.savefig(
        os.path.join(
            plot_folder, fname))


def plot_endpoint_cumulative_total_stacked(year=2050):

    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col=["Region"])
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios))

    units = {
        "Human health": "million DALYs",
        "Ecosystem quality": "thous. species years",
        "Resources": "trillion US$2000"
    }

    divs = {
        "Human health": 1e-6,
        "Ecosystem quality": 1e-3,
        "Resources": 1e-12
    }

    # fleet totals
    data["Category"] = data["Method"].apply(lambda x: eval(x)[1].capitalize())
    data["Method"] = data["Method"].apply(lambda x: eval(x)[2].capitalize())
    data = data.groupby(["Year", "Category", "Method", "Scenario"]).sum().drop(columns="score_pkm")

    data.reset_index(inplace=True)
    data["Scenario"] = data["Scenario"].str.replace("Budg1100_", "")
    data["Year"] = pd.to_datetime(data["Year"], format="%Y")
    data.set_index("Year", inplace=True)

    cum_data = data.groupby(["Category", "Method", "Scenario"]).resample("Y").mean()
    cum_data["total_score"] = cum_data["total_score"].interpolate()
    cum_data = cum_data.groupby(["Category", "Method", "Scenario"]).cumsum()

    ord_scens = ["ElecPush_LowD", "ElecPush", "H2Push", "ConvSyn", "Conv_LowD", "Conv"]

    cats = ["Human health", "Ecosystem quality", "Resources"]
    cum_data = cum_data.loc[:, :, :, "2050-12-31"]

    cum_data = cum_data.reset_index().set_index("Category")

    charts = []
    method_order={
        "Human health": [
            'Climate change', 'Particulate matter formation', 'Human toxicity',
            'Photochemical oxidant formation', 'Ionising radiation', 'Ozone depletion',
        ],
        "Ecosystem quality": ['Climate change', 'Agricultural land occupation', 'Urban land occupation',
                        'Marine ecotoxicity', 'Terrestrial ecotoxicity', 'Freshwater eutrophication',
                        'Marine eutrophication', 'Terrestrial acidification'],
        "Resources": [
            "Fossil depletion", "Metal depletion"]
    }
    schemes = ["set2", "set3", "set1"]
    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        sel = cum_data.loc[cat]
        sorter = dict(zip(method_order[cat], range(len(method_order[cat]), 0, -1)))
        sel["Method_sorter"] = sel["Method"].map(sorter)
        sel["total_score"] *= divs[cat]
        charts.append(alt.Chart(sel).mark_bar().encode(

            x=alt.X('Scenario:N', title=None, sort=ord_scens),

            y=alt.Y('sum(total_score):Q',
                    axis=alt.Axis(
                        grid=False,
                        title=units[cat])),
            color=alt.Color('Method:N',
                            sort=method_order[cat],
                            scale={
                                "scheme": schemes[no]}),
            order="Method_sorter"
            ))

    alt.hconcat(*charts)\
        .configure_view(
            strokeOpacity=0)\
        .resolve_scale(
            color="independent")\
        .configure_legend(
            orient='bottom',
            columns=1).save(
                os.path.join(
                    plot_folder, "endpoint_cumulative_absolute.png"))



def plot_contribution_analysis_treemap():

    year = 2050

    methods = [
        ('ReCiPe Endpoint (H,A) (obsolete)', 'resources', 'total'),
        ('ReCiPe Endpoint (I,A) (obsolete)', 'ecosystem quality', 'total'),
        ('ReCiPe Endpoint (I,A) (obsolete)', 'human health', 'total')
    ]

    for scenario, acstr in scencars.items():
        for method in methods:
            bw.projects.set_current(project_string(scenario))
            db = bw.Database(eidb_label(model, scenario, year))

            # find activity
            act = [a for a in db if
                   a["name"].startswith(acstr)
                   and a["location"] == region][0]

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            tot_score = lca.score

            results = []

            for exc in act.technosphere():
                if exc.input["name"] == "polyethylene production, high density, granulate":
                    exc["tag"] = "energy storage"
                lca.redo_lcia({exc.input: exc['amount']})
                results.append((exc["tag"], exc.input["name"], lca.score))

            results.append(
                ("direct", "driving emissions",
                 tot_score - sum(p[2] for p in results)))

            df = pd.DataFrame.from_records(
                results,
                columns=["parents", "children", "values"])

            df["all"] = act["name"]  # in order to have a single root node
            # remove negative values
            df = df[df["values"] > 0]

            # df.set_index(["all", "parent", "children"], inplace=True)
            fig = px.treemap(
                df, path=['all', 'parents', 'children'], values='values')
            # fig.show()
            fig.write_image(
                os.path.join(
                    plot_folder,
                    "treemap_{}_{}.png".format(method[1], scenario)),
                width=1024, height=768, scale=4)


def plot_endpoint_tech_relative(years = [2030, 2050]):

    ep_methods = [
        ('IPCC 2013', 'climate change', 'GWP 100a'),
        ('ReCiPe Endpoint (H,A) V1.13', 'Human health'),
        ('ReCiPe Endpoint (H,A) V1.13', 'Ecosystems'),
        ('ReCiPe Endpoint (H,A) V1.13', 'Resources')
    ]

    results = []
    # reference activity
    ref_scenario = "Budg1100_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))
    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"].startswith(scencars[ref_scenario])
        and a["location"] == region][0]

    for method in ep_methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        results.append(
            (ref_string, ref_year, method, lca.score))

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        for year in years:
            db = bw.Database(eidb_label(model, scenario, year))
            # find activity
            act = [a for a in db if
                   a["name"].startswith(acstr)
                   and a["location"] == region][0]
            for method in ep_methods:
                lca = bw.LCA({act: 1}, method)
                lca.lci()
                lca.lcia()

                results.append(
                    (scen_techs[scenario], year, method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["technology", "year", "method", "values"])

    df["method"] = df["method"].apply(lambda x: x[1])
    df.set_index(["year", "method", "technology"], inplace=True)

    fig, ax = plt.subplots(2, 1, figsize=(7, 4), gridspec_kw={
        'height_ratios': [1, 3],
        'hspace': 0.4})

    dfwide = df.loc[2050, "climate change"].rename(columns={"values": "Climate Change\n(IPCC 2013)"}).T
    dfwide = dfwide[tech_order]
    plot = dfwide.plot.barh(ax=ax[0], legend=False, width=0.6)
    dfwide = df.loc[2030, "climate change"].rename(columns={"values": "Climate Change"}).T
    plot = dfwide.plot.barh(ax=ax[0], legend=False, width=0.6, alpha=0.6)
    ax[0].set_xlabel("GWP 100a [kg CO2-eq.]")
    ax[0].set_ylabel("")
    ax[0].invert_yaxis()
    ax[0].text(-0.15, 1., "A", transform=ax[0].transAxes,
            size=14, weight='bold')

    df = df.query("method != 'climate change'")

    df = df.div(df.loc[ref_year, :, ref_string], axis="index")
    df.reset_index(["technology"], inplace=True)


    # 2030, symbols
    dfwide = df.loc[2050].pivot(columns="technology", values="values")\
        .reindex(["Resources", "Ecosystems", "Human health"]) * 100
    plot = dfwide.plot.barh(ax=ax[1], legend=False, width=0.6)

    dfwide = df.loc[2030].pivot(columns="technology", values="values")\
        .reindex(["Resources", "Ecosystems", "Human health"]) * 100
    dfwide.plot.barh(ax=ax[1], legend=False, width=0.6, alpha=0.6)
    handles, labels = ax[1].get_legend_handles_labels()

    ax[1].invert_yaxis()
    labels = ["2050: " + labels[0]] + ["      " + l for l in labels[1:4]]\
        + ["2030: " + labels[4]] + ["      " + l for l in labels[5:]]

    lgd = fig.legend(handles, labels, loc='upper left', bbox_to_anchor=(0.95, 0.9), ncol=1,
              borderaxespad=0, frameon=False)

    ax[1].set_xlabel("rel. impact [%]")
    ax[1].set_ylabel("endpoint")
    ax[1].text(-0.15, 1., "B", transform=ax[1].transAxes,
               size=14, weight='bold')
    # fig.suptitle("Impacts compared to an average car in 2020")
    # fig.tight_layout()
    fname = ("endpoint_tech_relative.png")
    fig.savefig(
        os.path.join(
            plot_folder, fname),
        bbox_extra_artists=(lgd,),
        bbox_inches="tight")


def plot_midpoint_tech_absolute(years = [2020, 2050]):
    results = []
    methods = [m for m in bw.methods if 'ReCiPe Endpoint (H) V1.13' == m[0]
               and not m[2] == "ozone depletion"
               and not m[2] == "natural land transformation"]
    units = {
        "Human health": "DALYs",
        "Ecosystem quality": "species years",
        "Resources": "US$2000"
    }

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        for year in years:
            db = bw.Database(eidb_label(model, scenario, year))
            # find activity
            act = [a for a in db if
                   a["name"].startswith(acstr)
                   and a["location"] == region][0]
            for method in methods:
                lca = bw.LCA({act: 1}, method)
                lca.lci()
                lca.lcia()

                results.append(
                    (year, scen_techs[scenario], method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["year", "technology", "method", "values"])

    df["category"] = df["method"].apply(lambda x: x[1].capitalize())
    df["method"] = df["method"].apply(lambda x: x[2].capitalize())

    df.set_index(["year", "category", "method"], inplace=True)

    fig, ax = plt.subplots(
        1, 3, figsize=(9.5, 5.5), gridspec_kw={'width_ratios': [0.5, 0.8, 0.2], 'wspace': 0.5})
    # fig.suptitle("Impacts compared to an average car in 2020")

    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        dfwide = df.loc[years[0]].loc[cat].pivot(columns="technology", values="values") * 1e6
        dfwide.plot.bar(ax=ax[no], width=0.8, alpha=0.5, legend=False)
        dfwide = df.loc[years[1]].loc[cat].pivot(columns="technology", values="values") * 1e6
        dfwide.plot.bar(ax=ax[no], width=0.8, legend=False)
        ax[no].set_xlabel("")
        ax[no].set_ylabel(units[cat])
        ax[no].set_yscale("log")
        ax[no].yaxis.set_tick_params(length=0)
        ax[no].title.set_text(cat)
        # ax[no].set_xticklabels(
        #     ax[no].get_xticklabels(),
        #     rotation=45, ha="right")


    ax[0].yaxis.set_tick_params(length=2)
    handles, labels = ax[-1].get_legend_handles_labels()
    labels = ["{}: {}".format(years[0], labels[0])] + ["      " + l for l in labels[1:4]]\
        + ["{}: {}".format(years[1], labels[4])] + ["      " + l for l in labels[5:]]

    lgd = fig.legend(handles, labels, loc='upper left', bbox_to_anchor=(0.05, -0.35), ncol=4,
                     borderaxespad=0)

    # fig.tight_layout()
    fname = ("midpoint_tech_absolute.png")
    fig.savefig(
        os.path.join(
            plot_folder, fname),
        bbox_extra_artists=(lgd,),
        bbox_inches="tight")

def plot_midpoint_tech_absolute_stacked(years = [2020, 2050]):
    results = []
    bw.projects.set_current(project_string(next(iter(scencars))))

    methods = [m for m in bw.methods if 'ReCiPe Endpoint (H) V1.13' == m[0]]
    units = {
        "Human health": "DALYs/Mvkm",
        "Ecosystem quality": "species years lost/Mvkm",
        "Resources": "US$2000/Mvkm"
    }

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        for year in years:
            db = bw.Database(eidb_label(model, scenario, year))
            # find activity
            act = [a for a in db if
                   a["name"].startswith(acstr)
                   and a["location"] == region][0]
            for method in methods:
                lca = bw.LCA({act: 1}, method)
                lca.lci()
                lca.lcia()

                results.append(
                    (year, scen_techs[scenario], method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["year", "technology", "method", "values"])

    df["category"] = df["method"].apply(lambda x: x[1].capitalize())
    df["method"] = df["method"].apply(lambda x: x[2].capitalize())
    df.set_index("category", inplace=True)

    df["values"] *= 1e6
    charts = []
    method_order={
        "Human health": [
            'Climate change', 'Particulate matter formation', 'Human toxicity',
            'Photochemical oxidant formation', 'Ionising radiation', 'Ozone depletion',
        ],
        "Ecosystem quality": ['Climate change', 'Agricultural land occupation', 'Urban land occupation',
                        'Marine ecotoxicity', 'Terrestrial ecotoxicity', 'Freshwater eutrophication',
                        'Marine eutrophication', 'Terrestrial acidification'],
        "Resources": [
            "Fossil depletion", "Metal depletion"]
    }
    schemes = ["set1", "set2", "set3"]
    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        charts.append(alt.Chart(df.loc[cat]).mark_bar().encode(

            # tell Altair which field to group columns on
            x=alt.X('technology:N', title=None),

            # tell Altair which field to use as Y values and how to calculate
            y=alt.Y('sum(values):Q',
                    axis=alt.Axis(
                        grid=False,
                        title=units[cat])),

            # tell Altair which field to use to use as the set of columns to be  represented in each group
            column=alt.Column('year:N', title=cat),

            # tell Altair which field to use for color segmentation
            color=alt.Color('method:N',
                            sort=method_order[cat],
                            scale={
                                "scheme": schemes[no]}),
            ))

    alt.hconcat(*charts)\
        .configure_view(
            # remove grid lines around column clusters
            strokeOpacity=0)\
        .resolve_scale(
            color="independent")\
        .configure_legend(
            orient='bottom',
            columns=1)
        # .save(os.path.join(plot_folder, "midpoint_tech_absolute_stacked.png"))


def plot_midpoint_tech_relative(year = 2050):
    results = []
    # reference activity
    ref_scenario = "Budg1100_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))
    methods = [m for m in bw.methods if 'ReCiPe Endpoint (H) V1.13' == m[0]]
    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"].startswith(scencars[ref_scenario])
        and a["location"] == region][0]

    for method in methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        results.append(
            (ref_string, method, lca.score))

    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"].startswith(acstr)
               and a["location"] == region][0]
        for method in methods:
            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            results.append(
                (scen_techs[scenario], method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["technology", "method", "values"])

    df["category"] = df["method"].apply(lambda x: x[1].capitalize())
    df["method"] = df["method"].apply(lambda x: x[2].capitalize())

    df.set_index(["technology", "category", "method"], inplace=True)

    df = df.div(df.loc[ref_string], axis="index")
    df.reset_index(["technology"], inplace=True)

    fig, ax = plt.subplots(
        1, 3, sharey=True,
        figsize=(8, 5.5), gridspec_kw={'width_ratios': [0.6, 0.9, 0.2]})
    # fig.suptitle("Impacts compared to an average car in 2020")

    for no, cat in enumerate(["Human health", "Ecosystem quality", "Resources"]):
        dfwide = df.loc[cat].pivot(columns="technology", values="values")\
                            .drop(columns=ref_string) * 100

        dfwide.plot.bar(ax=ax[no], width=0.8, legend=False)
        ax[no].axhline(100.)
        ax[no].set_xlabel("")
        ax[no].set_ylabel("rel. impact (%)")
        ax[no].yaxis.set_tick_params(length=0)
        ax[no].title.set_text(cat)
        # ax[no].set_xticklabels(
        #     ax[no].get_xticklabels(),
        #     rotation=45, ha="right")


    ax[0].yaxis.set_tick_params(length=2)
    handles, labels = ax[-1].get_legend_handles_labels()

    fig.legend(handles[:4], labels[:4], loc='lower left', bbox_to_anchor= (0.1, 0.75))
    fig.tight_layout()
    fname = ("midpoint_tech_relative.png")
    fig.savefig(
        os.path.join(
            plot_folder, fname))

def plot_contribution_analysis_stackedbars_relative(year = 2050):

    results = []
    # reference activity
    ref_scenario = "Budg1100_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))
    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"].startswith(scencars[ref_scenario])
        and a["location"] == region][0]

    for method in selected_methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        tot_score = lca.score
        proc_score = []

        for exc in act.technosphere():
            if exc.input["name"] == "polyethylene production, high density, granulate":
                exc["tag"] = "energy storage"
            lca.redo_lcia({exc.input: exc['amount']})
            results.append(
                (ref_string, method, exc["tag"],
                 exc.input["name"], lca.score))
            proc_score.append(lca.score)

        results.append(
            (ref_string, method, "direct", "driving emissions",
             tot_score - sum(proc_score)))


    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"].startswith(acstr)
               and a["location"] == region][0]
        for method in selected_methods:

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            tot_score = lca.score
            proc_score = []

            for exc in act.technosphere():
                if exc.input["name"] == "polyethylene production, high density, granulate":
                    exc["tag"] = "energy storage"
                lca.redo_lcia({exc.input: exc['amount']})
                results.append(
                    (scen_techs[scenario], method, exc["tag"],
                     exc.input["name"], lca.score))
                proc_score.append(lca.score)

            results.append(
                (scen_techs[scenario], method, "direct", "driving emissions",
                 tot_score - sum(proc_score)))

    df = pd.DataFrame.from_records(
        results,
        columns=["scenario", "method", "tag", "activity", "values"])
    df.reset_index(inplace=True)
    df["method"] = df["method"].apply(lambda x: "{}, {}".format(
        x[1].capitalize(), x[2]))

    df.to_csv("output/scenario_comparison_tagged.csv")
    ## plot in R

    df = df.groupby(["scenario", "method", "tag"]).sum().drop(columns="index")
    norm = df.loc[ref_string].groupby("method").sum()
    df /= norm
    df = df.loc[tech_order].reset_index()
    alt.Chart(df).mark_bar().encode(
        x=alt.X('scenario:N', sort=tech_order),
        y=alt.Y('sum(values):Q',
                axis=alt.Axis(
                    grid=False)),
        color=alt.Color('tag:N'),
        column=alt.Column('method:N',
                          header=alt.Header(labelAngle=60)))



def plot_tech_heatmap_relative(year = 2050):

    results = []
    # reference activity
    ref_scenario = "Budg1100_Conv"
    ref_year = 2020
    ref_string = "{}, {}".format(ref_scenario, ref_year)
    bw.projects.set_current(project_string(ref_scenario))

    methods = [m for m in bw.methods if m[0] == "ILCD 2.0 2018 midpoint"
                        and m[1] != "climate change"]
    methods.append(('IPCC 2013', 'climate change', 'GWP 100a'))

    db = bw.Database(eidb_label(model, ref_scenario, ref_year))
    act = [
        a for a in db if
        a["name"].startswith(scencars[ref_scenario])
        and a["location"] == region][0]

    for method in methods:
        lca = bw.LCA({act: 1}, method)
        lca.lci()
        lca.lcia()

        results.append(
            (ref_string, method, lca.score))


    for scenario, acstr in scencars.items():
        bw.projects.set_current(project_string(scenario))
        db = bw.Database(eidb_label(model, scenario, year))
        # find activity
        act = [a for a in db if
               a["name"].startswith(acstr)
               and a["location"] == region][0]
        for method in methods:

            lca = bw.LCA({act: 1}, method)
            lca.lci()
            lca.lcia()

            results.append(
                (scen_techs[scenario], method, lca.score))

    df = pd.DataFrame.from_records(
        results,
        columns=["technology", "method", "values"])
    df.reset_index(inplace=True)
    df["method"] = df["method"].apply(lambda x: "{}, {}".format(
        x[1].capitalize(), x[2]))
    df = df.set_index("method").drop(columns="index")

    scen_ord = ['BEV', 'FCEV', 'ICEV(synth. petrol)', 'ICEV(petrol)', ref_string]

    dfwide = df.pivot(columns="technology", values="values")[scen_ord]
    dfnorm = dfwide.div(dfwide[ref_string], axis="index").drop(columns=ref_string)
    fig, ax = plt.subplots(figsize=(8, 5))

    sns.heatmap(dfnorm, ax=ax, vmax=2, cmap='RdYlGn_r',
                annot=True, cbar=False, xticklabels=True, yticklabels=True)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=60, fontsize = 8)
    # ax.figure.subplots_adjust(bottom=0.2, left=0.2)
    fig.suptitle("Impacts in 2050 compared to an average car in 2020")
    fig.tight_layout()
    fname = ("midpoint_tech_heatmap_{}.png"
             .format(year))
    ax.figure.savefig(
        os.path.join(
            plot_folder, fname))



materials = ["Lithium", "Cobalt", "Platinum"]

def plot_fleet_materials():
    from lcatool import years, mifs

    # per-capita consumption data from Kaplinsky, R. et al (2010).
    cons = {"Iron": 400, "Copper": 12, "Aluminium": 17}

    with PdfPages(
            os.path.join(
                plot_folder,
                "fleet_materials_line.pdf")) as pdf:
        for material in materials:
            fig, axs = plt.subplots(1, 2, figsize=[12, 5])

            data = pd.concat((
                pd.read_csv(
                    get_storage_file_path(sc, "materials"),
                    header=0,
                    names=["Year", "Region", "Material", "value"],
                    index_col=["Region", "Material", "Year"])
                .loc[(region, material)]
                .assign(Scenario=sc)
                for sc in scenarios))

            ax = axs[0]
            ax.set_ylabel("{} (kg)".format(material))
            # fleet totals
            data = data.pivot(columns="Scenario", values="value") * 1e-6 # population is in million
            # per capita
            data = data.div(pop_data["value"], axis="index")
            data.plot(ax=ax, title="{} demand per capita".format(material),
                      legend=False)
            if material in cons:
                ax.axhline(cons[material], color='r')

            ax = axs[1]
            ax.set_ylabel("{} (kg)".format(material))
            cum_data = pd.merge(
                pd.DataFrame({"Year": range(2020, 2050)}),
                data.reset_index(), how="outer").interpolate()
            cum_data = cum_data.set_index("Year").cumsum()

            cum_data.plot(
                ax=ax, title="{} demand, cumulative".format(material))
            plt.tight_layout()
            pdf.savefig()
            plt.close()


def plot_fleet_midpoint_per_capita():
    with PdfPages(
            os.path.join(
                plot_folder,
                "midpoint_perCapita_line.pdf")) as pdf:
        data = pd.concat((
            pd.read_csv(
                get_storage_file_path(sc, "LDV_LCA"),
                index_col="Region")
            .loc[region]
            .assign(Scenario=sc)
            for sc in scenarios))

        data = data.groupby(["Method", "Year", "Scenario"]).sum()

        methods = data.index.levels[0]

        fig, axs = plt.subplots(4, 4, sharex=True, figsize=[14, 16])
        fig.suptitle("Full fleet, per-capita impact in {}"
                     .format(region))
        for idx, method in enumerate(methods):
            toplot = data.loc[method]
            toplot.reset_index(inplace=True)
            toplot.set_index("Year", inplace=True)

            # fleet totals
            toplot = toplot.pivot(columns="Scenario", values="total_score") * 1e-6 # population is in million
            toplot = toplot.div(pop_data["value"], axis="index")

            ax = axs[int(idx / 4), idx % 4]
            ax.set_ylabel(units[eval(method)])

            toplot.plot(ax=ax, title=eval(method)[2], legend=False)
            handles, labels = ax.get_legend_handles_labels()
            # reference per-capita impact
            ax.axhline(norm_fac[eval(method)], color='r')

        fig.legend(handles, labels, loc="upper right")
        fig.tight_layout()
        pdf.savefig()
        plt.close()


def plot_fleet_midpoint_per_capita_normalized():
    with PdfPages(
            os.path.join(
                plot_folder,
                "midpoint_perCapita_normalized_line.pdf")) as pdf:
        data = pd.concat((
            pd.read_csv(
                get_storage_file_path(sc, "LDV_LCA"),
                index_col="Region")
            .loc[region]
            .assign(Scenario=sc)
            for sc in scenarios))

        data = data.groupby(["Method", "Year", "Scenario"]).sum()

        methods = data.index.levels[0]

        fig, axs = plt.subplots(4, 4, sharex=True, figsize=[14, 16])
        fig.suptitle("Full fleet, per-capita impact in {}"
                     .format(region))
        for idx, method in enumerate(methods):
            toplot = data.loc[method]
            toplot.reset_index(inplace=True)
            toplot.set_index("Year", inplace=True)

            # fleet totals
            toplot = toplot.pivot(columns="Scenario", values="total_score") * 1e-6 # population is in million
            toplot = toplot.div(pop_data["value"]*norm_fac[eval(method)], axis="index")

            ax = axs[int(idx / 4), idx % 4]
            ax.set_ylabel("impact rel. to avg. impact per capita")

            toplot.plot(ax=ax, title=eval(method)[2], legend=False)
            handles, labels = ax.get_legend_handles_labels()

        fig.legend(handles, labels, loc="upper right")
        fig.tight_layout()
        pdf.savefig()
        plt.close()


def plot_fleet_midpoint_per_capita_bar_normalized(year=2050):
    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col="Region")
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns="score_pkm")

    data = data.groupby(["Year", "Method", "Scenario"]).sum("total_score")
    data = data.merge(pop_data, left_index=True, right_index=True)
    data = data.loc[year]

    methods = data.index.levels[0]

    data.reset_index(inplace=True)
    data["Method"] = data["Method"].apply(eval)
    data["Scenario"] = data["Scenario"].str.replace("Budg1100_", "")

    data.set_index("Method", inplace=True)
    data = data.loc[selected_methods]

    norms = pd.DataFrame.from_dict(norm_fac, orient="index",
                                   columns=["normfac"])

    data = data.merge(norms, left_index=True, right_index=True)
    data["percap_norm"] = data["total_score"] / (data["value"] * 1e6 * data["normfac"])
    data.index.name = "Method"
    data.drop(columns=["total_score", "value", "normfac"], inplace=True)

    data.reset_index(inplace=True)

    data["Method"] = data["Method"].apply(lambda x: "{}\n{}".format(
        x[1].capitalize(), x[2].capitalize()))

    data = data[~data.Scenario.str.contains("_LowD")]
    scen_ord = ['ConvSyn', 'H2Push', 'Conv', 'ElecPush']

    vardt = data.pivot(index="Method", columns="Scenario", values="percap_norm")[scen_ord]

    methods = ['Climate change\nGwp 100a',
               'Human health\nRespiratory effects, inorganics',
               'Human health\nNon-carcinogenic effects',
               'Human health\nCarcinogenic effects',
               'Resources\nMinerals and metals',
               'Resources\nDissipated water',
               'Resources\nLand use']


    vardt = vardt.loc[methods[::-1]]

    vardt.plot.barh(
        title=("Midpoint Impacts per Cap. relative to avg. Impacts"),
        figsize=(9, 6), legend="reverse", width=0.85, colormap="Paired")
    # plt.xscale("log")
    plt.axvline(1)
    # plt.xticks([0.01, 0.1, 0.5, 1, 2, 4],
    #            [1, 10, 50, 100, 200, 400])
    # plt.xlabel("%")
    plt.tight_layout()
    fname = ("midpoint_perCap_normalized_{}_bar.png"
             .format(year))
    plt.savefig(
        os.path.join(
            plot_folder, fname))


def plot_fleet_midpoint_heatmap_normalized(year=2050):
    data = pd.concat((
        pd.read_csv(
            get_storage_file_path(sc, "LDV_LCA"),
            index_col="Region")
        .loc[region]
        .assign(Scenario=sc)
        for sc in scenarios)).drop(columns="score_pkm")

    data = data.groupby(["Year", "Method", "Scenario"]).sum("total_score")
    data = data.merge(pop_data, left_index=True, right_index=True)
    data = data.loc[year]

    methods = data.index.levels[0]

    data.reset_index(inplace=True)
    data["Method"] = data["Method"].apply(eval)
    data["Scenario"] = data["Scenario"].str.replace("Budg1100_", "")

    data.set_index("Method", inplace=True)
    # data = data.loc[selected_methods]

    norms = pd.DataFrame.from_dict(norm_fac, orient="index",
                                   columns=["normfac"])

    data = data.merge(norms, left_index=True, right_index=True)
    data["percap_norm"] = data["total_score"] / (data["value"] * 1e6 * data["normfac"])
    data.index.name = "Method"
    data.drop(columns=["total_score", "value", "normfac"], inplace=True)

    data.reset_index(inplace=True)

    data["Method"] = data["Method"].apply(lambda x: "{}, {}".format(
        x[1].capitalize(), x[2]))

    data = data[~data.Scenario.str.contains("_LowD")]
    scen_ord = ['ElecPush', 'H2Push', 'ConvSyn', 'Conv']

    vardt = data.pivot(index="Method", columns="Scenario", values="percap_norm")[scen_ord]

    fig, ax = plt.subplots(figsize=(8, 5))

    sns.heatmap(vardt, vmax=2, ax=ax, cmap='RdYlGn_r',
                annot=True, cbar=False, xticklabels=True, yticklabels=True)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=60, fontsize = 8)
    # ax.figure.subplots_adjust(bottom=0.2, left=0.2)
    fig.suptitle("Fleet impacts in 2050 compared to avg. impacts per capita 2010 ")
    fig.tight_layout()
    fname = ("midpoint_fleet_heatmap_{}.png"
             .format(year))
    ax.figure.savefig(
        os.path.join(
            plot_folder, fname))


def plot_fleet_materials_normalized():
    materials = ["Iron", "Aluminium", "Gold", "Copper", "Platinum"]
    years = [2030, 2050]
    with PdfPages(
            os.path.join(
                plot_folder,
                "fleet_materials_normalized_bars.pdf")) as pdf:
        for material in materials:
            data = pd.concat((
                pd.read_csv(
                    get_storage_file_path(sc, "materials"),
                    header=0,
                    names=["Year", "Region", "Material", "value"],
                    index_col=["Region", "Material", "Year"])
                .loc[(region, material)]
                .assign(Scenario=sc)
                for sc in scenarios))

            # fleet totals
            data = data.pivot(columns="Scenario", values="value")
            data /= data.loc[2020]

            data.loc[years].plot(
                title="{} demand compared to 2020".format(material),
                kind="bar")
            pdf.savefig()
            plt.close()


if __name__ == '__main__':
    # print("plot_endpoint_cumulative_total_bar()")
    # plot_endpoint_cumulative_total_bar()
    # print("plot_contribution_analysis_stackedbars_relative(year = 2050)")
    # plot_contribution_analysis_stackedbars_relative(year = 2050)
    print("plot_midpoint_tech_relative(2050)")
    plot_midpoint_tech_relative(2050)
    print("plot_endpoint_cumulative_total_stacked()")
    plot_endpoint_cumulative_total_stacked()
    print("plot_midpoint_tech_absolute_stacked()")
    plot_midpoint_tech_absolute_stacked()
