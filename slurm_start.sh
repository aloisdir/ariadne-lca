#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=lca-calculations
#SBATCH --output=log-%j.out
#SBATCH --mail-type=END
#SBATCH --mem=60000

source /p/tmp/aloisdir/lca-env/bin/activate
conda-unpack

python lcatool.py -c
python lcatool.py --export
# python lcatool.py -s
