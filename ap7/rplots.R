library(data.table)
library(ggplot2)
library(stringr)

dt = fread("Alois_Fahrzeugkilometer.csv", skip=3, drop=c(1, 2, 4, 6, 9, 12, 15))
dt <- transpose(dt)
colnames(dt) <- as.character(head(dt, 1))
dt <- dt[Jahr != "Jahr"]
dt <- melt(dt, id.vars=c("Jahr", "Szenario"), variable.name="Technologie")
dt[, value := as.numeric(value)]
dt <- dt[Technologie != "Gesamt"]
dt[, Technologie := as.character(Technologie)]
dt[, Jahr := as.numeric(as.character(Jahr))]

## load footprints / vkm
fp <- fread("~/git/ariadne-lca/output/recycling_high/LDV_LCA_Elec_dom.csv")
fp <- fp[Year %in% dt$Jahr & Region == "DEU"][, Region := NULL]
fp[, Variable := gsub("ES|Transport|VKM|Pass|Road|LDV|", "", Variable, fixed=T)]
setnames(fp, c("Year", "Variable"), c("Jahr", "Technologie"))

fp[Technologie == "Hybrid Electric", Technologie := "PHEV"]
fp[Technologie == "Liquids", Technologie := "Verbrenner"]
fp[, Jahr := as.numeric(as.character(Jahr))]

fp <- fp[dt, on=c("Jahr", "Technologie"), allow.cartesian=T]
fp[, c("gesamt", "direkt") := list(score_pkm * value, score_pkm_direct * value)]

fp[, Method := gsub("\\((.*)\\)", "\\1", Method)][, c("class", "category", "Method") := lapply(tstrsplit(Method, ","), function(cat){
  gsub("\\s*'(.+)'\\s*", "\\1", cat)})]
fp[category == "CO2 emissions", category := "climate change"]

trans = fread(text="Method,Methode
        human toxicity,Gifte
        photochemical oxidant formation,Sommersmog
        particulate matter formation,Feinstaub
        agricultural land occupation,Landverbrauch (Land)
        urban land occupation,Landverbrauch (Stadt)
        terrestrial ecotoxicity,Gifte (Böden)
        freshwater ecotoxicity,Gifte (Grundwasser)
        marine ecotoxicity,Gifte (Gewässer)
        terrestrial acidification,Bodenversäuerung
        marine eutrophication,Gewässereutrophierung
")

fp <- fp[trans, on="Method"]

## human health
toplot <- fp[category == "human health"]#[, c("gesamt", "direkt") := list(gesamt, direkt)]
fwrite(toplot[, .(gesamt=sum(gesamt), direkt=sum(direkt)), by=c("Szenario", "Jahr", "Technologie")], "human_health.csv")

ggplot(toplot[gesamt > 1], aes(x=Szenario, y=`gesamt`, fill=Methode)) +
  geom_bar(position="stack", stat="identity") +
  facet_wrap(~Jahr, scales="free_x") +
  labs(y="Gesundheitswirkung [Tsd. DALYs]", title="Gesundheitwirkungen der Flotte (gesamt)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

ggsave("human_health_total.png")

ggplot(toplot[direkt > 0.1], aes(x=Szenario, y=`direkt`, fill=Methode)) +
  geom_bar(position="stack", stat="identity") +
  facet_wrap(~Jahr, scales="free_x") +
  labs(y="Gesundheitswirkung [Tsd. DALYs]", title="Gesundheitwirkungen der Flotte (direkt)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

ggsave("human_health_direct.png")

toplot <- fp[category == "ecosystem quality"]
fwrite(toplot[, .(gesamt=sum(gesamt), direkt=sum(direkt)), by=c("Szenario", "Jahr", "Technologie")], "ecosystem_quality.csv")

ggplot(toplot, aes(x=Szenario, y=`gesamt`, fill=Methode)) +
  geom_bar(position="stack", stat="identity") +
  facet_wrap(~Jahr, scales="free_x") +
  labs(y="Umweltwirkung [Verl. Artenjahre]", title="Umweltwirkungen der Flotte (gesamt)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

ggsave("ecosystem_total.png")

ggplot(toplot, aes(x=Szenario, y=`direkt`, fill=Methode)) +
  geom_bar(position="stack", stat="identity") +
  facet_wrap(~Jahr, scales="free_x") +
  labs(y="Umweltwirkung [Verl. Artenjahre]", title="Umweltwirkungen der Flotte (direkt)") +
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

ggsave("ecosystem_direct.png")


## Ressourcen

res <- fread("~/git/ariadne-lca/output/recycling_high/materials_Elec_dom.csv")
res <- res[Year %in% dt$Jahr & Region == "DEU"][, Region := NULL]
res[, Variable := gsub("ES|Transport|VKM|Pass|Road|LDV|", "", Variable, fixed=T)]
setnames(res, c("Year", "Variable"), c("Jahr", "Technologie"))

res[Technologie == "Hybrid Electric", Technologie := "PHEV"]
res[Technologie == "Liquids", Technologie := "Verbrenner"]
res[, Jahr := as.numeric(as.character(Jahr))]

res <- res[dt, on=c("Jahr", "Technologie"), allow.cartesian=T]
res[, total := per_pkm * value * 1e-3]

## selected materials

trans = fread(text="Material,Material2
        Copper,Kupfer
        Gold,Gold
        Aluminium,Aluminium
        Lithium,Lithium
        Cobalt,Kobalt
")

res <- res[trans, on="Material"]
fwrite(res, "resources.csv")

for(material in unique(res$Material2)){
  toplot <- res[Material2 == material]

  ggplot(toplot, aes(x=Szenario, y=`total`)) +
    geom_bar(position="stack", stat="identity") +
    facet_wrap(~Jahr, scales="free_x") +
    labs(y="Rohstoffverbrauch [t]", title=sprintf("%s Flottenverbrauch", material)) +
    theme_minimal() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

  ggsave(sprintf("material_%s.png", material))
}
